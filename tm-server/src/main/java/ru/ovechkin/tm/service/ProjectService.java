package ru.ovechkin.tm.service;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.ovechkin.tm.api.repository.IProjectRepository;
import ru.ovechkin.tm.api.service.IProjectService;
import ru.ovechkin.tm.exeption.empty.*;
import ru.ovechkin.tm.entity.Project;
import ru.ovechkin.tm.exeption.unknown.ProjectUnknownException;

import java.util.List;

public class ProjectService extends AbstractService<Project> implements IProjectService {

    @NotNull
    private final IProjectRepository projectRepository;

    public ProjectService(@NotNull final IProjectRepository projectRepository) {
        super(projectRepository);
        this.projectRepository = projectRepository;

    }

    @Override
    public void add(@Nullable final String userId, @Nullable final Project project) {
        if (userId == null || userId.isEmpty()) throw new UserEmptyException();
        if (project == null) return;
        projectRepository.add(userId, project);
    }

    @Override
    public void create(@Nullable final String userId, @Nullable final String name) {
        if (userId == null || userId.isEmpty()) throw new UserEmptyException();
        if (name == null || name.isEmpty()) throw new NameEmptyException();
        @NotNull final Project project = new Project();
        project.setName(name);
        projectRepository.add(userId, project);
    }

    @Override
    public void create(@Nullable final String userId, @Nullable final String name, @Nullable final String description) {
        if (userId == null || userId.isEmpty()) throw new UserEmptyException();
        final Project project = new Project();
        if (name == null || name.isEmpty()) throw new NameEmptyException();
        project.setName(name);
        if (description == null || description.isEmpty()) throw new DescriptionEmptyException();
        project.setDescription(description);
        projectRepository.add(userId, project);
    }

    @NotNull
    @Override
    public List<Project> findUserProjects(@Nullable final String userId) {
        if (userId == null || userId.isEmpty()) throw new UserEmptyException();
        return projectRepository.findUserProjects(userId);
    }

    @Override
    public void removeAllProjects(@Nullable final String userId) {
        if (userId == null || userId.isEmpty()) throw new UserEmptyException();
        if (projectRepository.findAll() == null || projectRepository.findAll().isEmpty()) {
            throw new ProjectListEmptyException();
        }
        projectRepository.removeAll(userId);
    }

    @Nullable
    @Override
    public Project findProjectById(@Nullable final String userId, @Nullable final String id) {
        if (userId == null || userId.isEmpty()) throw new UserEmptyException();
        if (id == null || id.isEmpty()) throw new IdEmptyException();
        if (projectRepository.findAll() == null || projectRepository.findAll().isEmpty()) {
            throw new ProjectListEmptyException();
        }
        return projectRepository.findById(userId, id);
    }

    @Nullable
    @Override
    public Project findProjectByIndex(@Nullable final String userId, @Nullable final Integer index) {
        if (userId == null || userId.isEmpty()) throw new UserEmptyException();
        if (index == null || index <= 0) throw new IndexEmptyException();
        if (projectRepository.findAll() == null || projectRepository.findAll().isEmpty()) {
            throw new ProjectListEmptyException();
        }
        return projectRepository.findByIndex(userId, index);
    }

    @Nullable
    @Override
    public Project findProjectByName(@Nullable final String userId, @Nullable final String name) {
        if (userId == null || userId.isEmpty()) throw new UserEmptyException();
        if (name == null || name.isEmpty()) throw new NameEmptyException();
        if (projectRepository.findAll() == null || projectRepository.findAll().isEmpty()) {
            throw new ProjectListEmptyException();
        }
        return projectRepository.findByName(userId, name);
    }

    @NotNull
    @Override
    public Project updateProjectById(
            @Nullable final String userId,
            @Nullable final String id,
            @Nullable final String name,
            @Nullable final String description
    ) {
        if (userId == null || userId.isEmpty()) throw new UserEmptyException();
        if (id == null || id.isEmpty()) throw new IdEmptyException();
        if (name == null || name.isEmpty()) throw new NameEmptyException();
        if (description == null || description.isEmpty()) throw new DescriptionEmptyException();
        if (projectRepository.findAll() == null || projectRepository.findAll().isEmpty()) {
            throw new ProjectListEmptyException();
        }
        final Project project = findProjectById(userId, id);
        if (project == null) throw new ProjectUnknownException();
        project.setId(id);
        project.setName(name);
        project.setDescription(description);
        return project;
    }

    @NotNull
    @Override
    public Project updateProjectByIndex(
            @Nullable final String userId,
            @Nullable final Integer index,
            @Nullable final String name,
            @Nullable final String description
    ) {
        if (userId == null || userId.isEmpty()) throw new UserEmptyException();
        if (index == null || index <= 0) throw new IndexEmptyException();
        if (name == null || name.isEmpty()) throw new NameEmptyException();
        if (description == null || description.isEmpty()) throw new DescriptionEmptyException();
        if (projectRepository.findAll() == null || projectRepository.findAll().isEmpty()) {
            throw new ProjectListEmptyException();
        }
        final Project project = findProjectByIndex(userId, index);
        if (project == null) throw new ProjectUnknownException();
        project.setName(name);
        project.setDescription(description);
        return project;
    }

    @Nullable
    @Override
    public Project removeProjectById(@Nullable final String userId, @Nullable final String id) {
        if (userId == null || userId.isEmpty()) throw new UserEmptyException();
        if (id == null || id.isEmpty()) throw new IdEmptyException();
        if (projectRepository.findAll() == null || projectRepository.findAll().isEmpty()) {
            throw new ProjectListEmptyException();
        }
        return projectRepository.removeById(userId, id);
    }

    @Nullable
    @Override
    public Project removeProjectByIndex(@Nullable final String userId, @Nullable final Integer index) {
        if (userId == null || userId.isEmpty()) throw new UserEmptyException();
        if (index == null || index <= 0) throw new IndexEmptyException();
        if (projectRepository.findAll() == null || projectRepository.findAll().isEmpty()) {
            throw new ProjectListEmptyException();
        }return projectRepository.removeByIndex(userId, index);
    }

    @Nullable
    @Override
    public Project removeProjectByName(@Nullable final String userId, @Nullable final String name) {
        if (userId == null || userId.isEmpty()) throw new UserEmptyException();
        if (name == null || name.isEmpty()) throw new NameEmptyException();
        if (projectRepository.findAll() == null || projectRepository.findAll().isEmpty()) {
            throw new ProjectListEmptyException();
        }return projectRepository.removeByName(userId, name);
    }

}