package ru.ovechkin.tm.service;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.ovechkin.tm.api.repository.IUserRepository;
import ru.ovechkin.tm.api.service.IUserService;
import ru.ovechkin.tm.entity.User;
import ru.ovechkin.tm.exeption.empty.*;
import ru.ovechkin.tm.enumirated.Role;
import ru.ovechkin.tm.exeption.other.NotLoggedInException;
import ru.ovechkin.tm.exeption.other.SomethingWentWrongException;
import ru.ovechkin.tm.exeption.other.UserDoesNotExistException;
import ru.ovechkin.tm.exeption.unknown.IdUnknownException;
import ru.ovechkin.tm.exeption.unknown.LoginUnknownException;
import ru.ovechkin.tm.exeption.user.*;
import ru.ovechkin.tm.util.HashUtil;

public class UserService extends AbstractService<User> implements IUserService {

    @NotNull
    private final IUserRepository userRepository;

    @NotNull
    public UserService(@NotNull IUserRepository userRepository) {
        super(userRepository);
        this.userRepository = userRepository;
    }

    @Nullable
    @Override
    public User findById(@Nullable final String id) {
        if (id == null || id.isEmpty()) throw new IdEmptyException();
        @Nullable final User user = userRepository.findById(id);
        return user;
    }

    @Nullable
    @Override
    public User findByLogin(@Nullable final String login) {
        if (login == null || login.isEmpty()) throw new LoginEmptyException();
        @Nullable final User user = userRepository.findByLogin(login);
        return user;
    }

    @NotNull
    @Override
    public User removeUser(@Nullable final User user) {
        if (user == null) throw new UserEmptyException();
        return userRepository.removeUser(user);
    }

    @Override
    @NotNull
    public User removeById(@Nullable final String adminId, @Nullable final String id) {
        if (adminId == null || adminId.isEmpty()) throw new IdEmptyException();
        if (id == null || id.isEmpty()) throw new IdEmptyException();
        @Nullable final User user = findById(id);
        if (user == null) throw new UserDoesNotExistException();
        final User currentUser = findById(adminId);
        if (user.getId().equals(currentUser.getId())) throw new SelfRemovingException();
        if (user.getRole() == Role.ADMIN) throw new AdminRemovingException();
        if (currentUser.getRole() != Role.ADMIN) throw new AccessDeniedException();
        userRepository.removeById(id);
        return user;
    }

    @NotNull
    @Override
    public User removeByLogin(@Nullable final String userId, @Nullable final String login) {
        if (userId == null || userId.isEmpty()) throw new IdEmptyException();
        if (login == null || login.isEmpty()) throw new LoginEmptyException();
        @Nullable final User user = findByLogin(login);
        if (user == null) throw new UserDoesNotExistException();
        final User currentUser = findById(userId);
        if (user.getLogin().equals(currentUser.getLogin())) throw new SelfRemovingException();
        if (user.getRole() == Role.ADMIN) throw new AdminRemovingException();
        if (currentUser.getRole() != Role.ADMIN) throw new AccessDeniedException();
        userRepository.removeByLogin(login);
        return user;
    }

    @NotNull
    @Override
    public User create(@Nullable final String login, @Nullable final String password) {
        if (login == null || login.isEmpty()) throw new LoginEmptyException();
        if (password == null || password.isEmpty()) throw new PasswordEmptyException();
        final User user = new User();
        user.setRole(Role.USER);
        user.setLogin(login);
        user.setPasswordHash(HashUtil.salt(password));
        return userRepository.add(user);
    }

    @NotNull
    @Override
    public User create(@Nullable final String login, @Nullable final String password, @Nullable final String email) {
        if (login == null || login.isEmpty()) throw new LoginEmptyException();
        if (password == null || password.isEmpty()) throw new PasswordEmptyException();
        if (email == null || email.isEmpty()) throw new EmailEmptyException();
        final User user = create(login, password);
        if (user == null) throw new SomethingWentWrongException();
        user.setEmail(email);
        return user;
    }

    @NotNull
    @Override
    public User create(@Nullable final String login, @Nullable final String password, @Nullable final Role role) {
        if (login == null || login.isEmpty()) throw new LoginEmptyException();
        if (password == null || password.isEmpty()) throw new PasswordEmptyException();
        if (role == null) throw new RoleEmptyException();
        final User user = create(login, password);
        if (user == null) throw new UserDoesNotExistException();
        user.setRole(role);
        return user;
    }

    @NotNull
    @Override
    public User lockUserByLogin(@Nullable final String userId, @Nullable final String login) {
        if (userId == null || userId.isEmpty()) throw new IdEmptyException();
        if (login == null || login.isEmpty()) throw new LoginEmptyException();
        final User user = findByLogin(login);
        if (user == null) throw new UserDoesNotExistException();
        final User currentUser = findById(userId);
        if (currentUser == null) throw new UserDoesNotExistException();
        if (user.getLogin().equals(currentUser.getLogin())) throw new SelfBlockingException();
        if (user.getRole() == Role.ADMIN) throw new AdminBlockingException();
        if (currentUser.getRole() != Role.ADMIN) throw new AccessDeniedException();
        user.setLocked(true);
        return user;
    }

    @NotNull
    @Override
    public User unLockUserByLogin(@Nullable final String userId, @Nullable final String login) {
        if (userId == null || userId.isEmpty()) throw new IdEmptyException();
        if (login == null || login.isEmpty()) throw new LoginEmptyException();
        final User user = findByLogin(login);
        if (user == null) throw new UserDoesNotExistException();
        final User currentUser = findById(userId);
        if (currentUser == null) throw new AccessDeniedException();
        if (currentUser.getRole() != Role.ADMIN) throw new AccessDeniedException();
        user.setLocked(false);
        return user;
    }

}