package ru.ovechkin.tm.api.repository;

import org.jetbrains.annotations.NotNull;
import ru.ovechkin.tm.entity.AbstractEntity;

import java.util.List;

public interface IRepository <E extends AbstractEntity> {

    @NotNull
    List<E> findAll();

    @NotNull
    E merge(@NotNull E entity);

    void merge(@NotNull List<E> entities);

    void merge(@NotNull E... entities);

    void load(@NotNull List<E> entities);

    void load(@NotNull E... entities);

    void clear();

}