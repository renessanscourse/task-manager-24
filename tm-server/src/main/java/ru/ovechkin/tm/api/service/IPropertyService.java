package ru.ovechkin.tm.api.service;

import org.jetbrains.annotations.NotNull;

public interface IPropertyService {

    void init() throws Exception;

    @NotNull
    String getServiceHost();

    @NotNull
    Integer getServicePort();

    @NotNull
    String getSessionSalt();

    @NotNull
    Integer getSessionCycle();

}