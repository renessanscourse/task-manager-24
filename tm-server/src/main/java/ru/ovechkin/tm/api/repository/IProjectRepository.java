package ru.ovechkin.tm.api.repository;

import java.util.List;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.ovechkin.tm.entity.Project;

public interface IProjectRepository extends IRepository<Project> {

    void add(@NotNull String userId, @NotNull Project project);

    @NotNull
    List<Project> findUserProjects(@NotNull String userId);

    void removeAll(@NotNull String userId);

    @Nullable
    Project findById(@NotNull String userId, @NotNull String id);

    @Nullable
    Project findByIndex(@NotNull String userId, @NotNull Integer index);

    @Nullable
    Project findByName(@NotNull String userId, @NotNull String name);

    @Nullable
    Project removeById(@NotNull String userId, @NotNull String id);

    @Nullable
    Project removeByIndex(@NotNull String userId, @NotNull Integer index);

    @Nullable
    Project removeByName(@NotNull String userId, @NotNull String name);

}