package ru.ovechkin.tm.repository;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.ovechkin.tm.api.repository.IProjectRepository;
import ru.ovechkin.tm.api.repository.ISessionRepository;
import ru.ovechkin.tm.entity.Project;
import ru.ovechkin.tm.entity.Session;
import ru.ovechkin.tm.entity.Task;
import ru.ovechkin.tm.entity.User;
import ru.ovechkin.tm.exeption.other.AccessForbiddenException;
import ru.ovechkin.tm.exeption.unknown.IdUnknownException;

import java.util.ArrayList;
import java.util.List;

public final class SessionRepository extends AbstractRepository<Session> implements ISessionRepository {

    public void remove(@NotNull final Session session) {
        entities.remove(session);
    }

    @NotNull
    public List<Session> findByUserId(@Nullable final String userId) {
        if (userId == null || userId.isEmpty()) return new ArrayList<>();
        @Nullable final List<Session> sessions = findAll();
        @Nullable final List<Session> result = new ArrayList<>();
        for (@NotNull final Session session : sessions) {
            if (userId.equals(session.getUserId())) {
                result.add(session);
            }
        }
        return result;
    }

    public void removeByUserId(@Nullable final String userId) {
        @Nullable final List<Session> sessions = findByUserId(userId);
        for (@NotNull final Session session : sessions) {
            remove(session);
        }
    }

    @Nullable
    public Session findById(@NotNull final String id) {
        for (@NotNull final Session session : entities) {
            if (id.equals(session.getId())) return session;
        }
        return null;
    }

    public boolean contains(@NotNull final String id) {
        @Nullable final Session session = findById(id);
        return entities.contains(session);
    }

}