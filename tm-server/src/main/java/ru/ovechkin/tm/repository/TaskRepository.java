package ru.ovechkin.tm.repository;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.ovechkin.tm.api.repository.ITaskRepository;
import ru.ovechkin.tm.entity.Project;
import ru.ovechkin.tm.entity.Task;
import ru.ovechkin.tm.exeption.unknown.IdUnknownException;
import ru.ovechkin.tm.exeption.unknown.IndexUnknownException;
import ru.ovechkin.tm.exeption.unknown.NameUnknownException;

import java.util.ArrayList;
import java.util.Collection;
import java.util.List;

public final class TaskRepository extends AbstractRepository<Task> implements ITaskRepository {

    @Override
    public void add(@NotNull final String userId, @NotNull final Task task) {
        task.setUserId(userId);
        entities.add(task);
    }

    @NotNull
    @Override
    public List<Task> findAllUserTask(@NotNull final String userId) {
        @NotNull final List<Task> result = new ArrayList<>();
        for (@NotNull final Task task : entities) {
            if (userId.equals(task.getUserId())) result.add(task);
        }
        return result;
    }

    @Override
    public void removeAll(@NotNull final String userId) {
        @NotNull final List<Task> tasks = findAllUserTask(userId);
        this.entities.removeAll(tasks);
    }

    @Nullable
    @Override
    public Task findById(@NotNull final String userId, @NotNull final String id) {
        for (@NotNull final Task task : entities) {
            if (userId.equals(task.getUserId())) {
                if (id.equals(task.getId())) return task;
            }
        }
        return null;
    }

    @Nullable
    @Override
    public Task findByIndex(@NotNull final String userId,@NotNull final Integer index) {
        final @Nullable List<Task> userTasks = findAllUserTask(userId);
        for (@NotNull final Task task: userTasks) {
            if (userId.equals(task.getUserId())) {
                if ((userTasks.indexOf(task) + 1) == index) return task;
            }
        }
        return null;
    }

    @Nullable
    @Override
    public Task findByName(@NotNull final String userId,@NotNull final String name) {
        for (@NotNull final Task task : entities) {
            if (userId.equals(task.getUserId())) {
                if (name.equals(task.getName())) return task;
            }
        }
        return null;
    }

    @Nullable
    @Override
    public Task removeById(@NotNull final String userId,@NotNull final String id) {
        @Nullable final Task task = findById(userId, id);
        if (task == null) return null;
        entities.remove(task);
        return task;
    }

    @Nullable
    @Override
    public Task removeByIndex(@NotNull final String userId,@NotNull final Integer index) {
        @Nullable final Task task = findByIndex(userId, index);
        if (task == null) return null;
        entities.remove(task);
        return task;
    }

    @Nullable
    @Override
    public Task removeByName(@NotNull final String userId,@NotNull final String name) {
        @Nullable final Task task = findByName(userId, name);
        if (task == null) return null;
        entities.remove(task);
        return task;
    }

}