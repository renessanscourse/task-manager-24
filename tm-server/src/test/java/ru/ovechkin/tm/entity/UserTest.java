package ru.ovechkin.tm.entity;

import org.junit.Assert;
import org.junit.Test;
import ru.ovechkin.tm.enumirated.Role;

import static ru.ovechkin.tm.constant.TestConst.TEST_STRING;

public class UserTest {

    private final User user = new User();

    @Test
    public void testLogin() {
        Assert.assertNotNull(user.getLogin());
        user.setLogin(TEST_STRING);
        Assert.assertNotNull(user.getLogin());
        Assert.assertEquals(TEST_STRING, user.getLogin());
    }

    @Test
    public void testPasswordHash() {
        Assert.assertNotNull(user.getPasswordHash());
        user.setPasswordHash(TEST_STRING);
        Assert.assertNotNull(user.getPasswordHash());
        Assert.assertEquals(TEST_STRING, user.getPasswordHash());
    }

    @Test
    public void testEmail() {
        Assert.assertNotNull(user.getEmail());
        user.setEmail(TEST_STRING);
        Assert.assertNotNull(user.getEmail());
        Assert.assertEquals(TEST_STRING, user.getEmail());
    }

    @Test
    public void testFirstName() {
        Assert.assertNotNull(user.getFirstName());
        user.setFirstName(TEST_STRING);
        Assert.assertNotNull(user.getFirstName());
        Assert.assertEquals(TEST_STRING, user.getFirstName());
    }

    @Test
    public void testMiddleName() {
        Assert.assertNotNull(user.getMiddleName());
        user.setMiddleName(TEST_STRING);
        Assert.assertNotNull(user.getMiddleName());
        Assert.assertEquals(TEST_STRING, user.getMiddleName());
    }

    @Test
    public void testLastName() {
        Assert.assertNotNull(user.getLastName());
        user.setLastName(TEST_STRING);
        Assert.assertNotNull(user.getLastName());
        Assert.assertEquals(TEST_STRING, user.getLastName());
    }

    @Test
    public void testRole() {
        Assert.assertNotNull(user.getRole());
        Assert.assertEquals(Role.USER, user.getRole());
        user.setRole(Role.ADMIN);
        Assert.assertNotNull(user.getRole());
        Assert.assertEquals(Role.ADMIN, user.getRole());
    }

    @Test
    public void testLockedStatus() {
        Assert.assertNotNull(user.getLocked());
        user.setLocked(true);
        Assert.assertNotNull(user.getLocked());
        Assert.assertEquals(true, user.getLocked());
    }

}