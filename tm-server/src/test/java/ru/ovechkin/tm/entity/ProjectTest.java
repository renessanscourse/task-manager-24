package ru.ovechkin.tm.entity;


import org.junit.*;
import org.junit.rules.ErrorCollector;
import ru.ovechkin.tm.constant.TestConst;

//@FixMethodOrder(MethodSorters.NAME_ASCENDING)
public class ProjectTest {

    private final Project project = new Project();

    @Rule
    public ErrorCollector collector = new ErrorCollector();

    @Test(timeout = 5)
    public void testId() {
        try {
            Assert.assertNotNull(project.getId());
            project.setId(TestConst.TEST_STRING);
            Assert.assertEquals(TestConst.TEST_STRING, project.getId());
        } catch (Exception e) {
            collector.addError(e);
        }
    }

    @Test
    public void testProjectUserId() {
        project.setUserId(TestConst.TEST_STRING);
        Assert.assertNotNull(project.getUserId());
        Assert.assertEquals(TestConst.TEST_STRING, project.getUserId());
    }

    @Test
    public void testProjectName() {
        Assert.assertNotNull(project.getName());
        project.setName(TestConst.TEST_STRING);
        Assert.assertEquals(TestConst.TEST_STRING, project.getName());
    }

    @Test
    public void testProjectDescription() {
        Assert.assertNotNull(project.getDescription());
        project.setDescription(TestConst.TEST_STRING);
        Assert.assertEquals(TestConst.TEST_STRING, project.getDescription());
    }

}