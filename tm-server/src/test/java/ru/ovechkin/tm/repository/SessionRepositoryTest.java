package ru.ovechkin.tm.repository;

import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;
import ru.ovechkin.tm.api.repository.ISessionRepository;
import ru.ovechkin.tm.entity.Session;
import ru.ovechkin.tm.entitiesForTest.SessionsForTest;

public class SessionRepositoryTest {

    private final ISessionRepository sessionRepository = new SessionRepository();

    private final SessionsForTest sessionsForTest = new SessionsForTest();

    private final Session sessionForTest = sessionsForTest.getSessionOfTestUser();

    @Before
    public void prepareRepository() {
        sessionRepository.clear();
        sessionRepository.merge(sessionForTest);
    }

    @Test
    public void testRemove() {
        sessionRepository.clear();
        Assert.assertFalse(sessionRepository.contains(sessionForTest.getId()));
    }

    @Test
    public void testFindByUserId() {
        Assert.assertFalse(sessionRepository.findByUserId("").contains(sessionForTest));
        Assert.assertFalse(sessionRepository.findByUserId(null).contains(sessionForTest));
        Assert.assertTrue(sessionRepository.findByUserId(sessionForTest.getUserId()).contains(sessionForTest));
    }

    @Test
    public void testRemoveByUserId() {
        sessionRepository.removeByUserId(sessionForTest.getUserId());
        Assert.assertFalse(sessionRepository.contains(sessionForTest.getId()));
    }

    @Test
    public void testFindById() {
        Assert.assertEquals(sessionForTest, sessionRepository.findById(sessionForTest.getId()));
        sessionRepository.clear();
        Assert.assertNull(sessionRepository.findById(sessionForTest.getId()));
    }

    @Test
    public void testContains() {
        Assert.assertTrue(sessionRepository.contains(sessionForTest.getId()));
        sessionRepository.clear();
        Assert.assertFalse(sessionRepository.contains(sessionForTest.getId()));
    }

}