package ru.ovechkin.tm.repository;

import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;
import ru.ovechkin.tm.entity.Project;
import ru.ovechkin.tm.entitiesForTest.ProjectsForTest;

import java.util.ArrayList;
import java.util.List;

/**
 * Абстрактный репозиторий тестируется на основании проектного репозитория,
 * но можно выбрать любой другой репозиторий, поскольку каждый из них
 * наследует абстрактный
 */
public class AbstractRepositoryTest {

    private final AbstractRepository<Project> abstractRepository = new ProjectRepository();

    private final ProjectsForTest projectsForTest = new ProjectsForTest();

    private final Project project1 = projectsForTest.getProject1();

    private final Project project2 = projectsForTest.getProject2();

    private final Project project3 = projectsForTest.getProject3();

    public List<Project> listOfTestProjects() {
        return projectsForTest.getListOfTestProjects();
    }

    @Before
    public void clearRepository() {
        abstractRepository.clear();
    }

    @Test
    public void testClear() {
        abstractRepository.merge(listOfTestProjects());
        abstractRepository.clear();
        Assert.assertTrue(abstractRepository.findAll().isEmpty());
    }

    @Test
    public void testFindAll() {
        Assert.assertNotNull(abstractRepository.findAll());
        abstractRepository.merge(listOfTestProjects());
        Assert.assertEquals(listOfTestProjects(), abstractRepository.findAll());
    }

    @Test
    public void testMergeOneEntity() {
        abstractRepository.merge(project1);
        Assert.assertTrue(abstractRepository.findAll().contains(project1));
    }

    @Test
    public void testMergeListOfEntities() {
        abstractRepository.merge(listOfTestProjects());
        Assert.assertEquals(listOfTestProjects(), abstractRepository.findAll());
    }

    @Test
    public void testManyOfEntities() {
        abstractRepository.merge(project1, project2, project3);
        Assert.assertTrue(abstractRepository.findAll().contains(project1));
        Assert.assertTrue(abstractRepository.findAll().contains(project2));
        Assert.assertTrue(abstractRepository.findAll().contains(project3));
    }

    @Test
    public void testLoadListOfEntities() {
        abstractRepository.load(listOfTestProjects());
        Assert.assertEquals(listOfTestProjects(), abstractRepository.findAll());
    }

    @Test
    public void testLoadManyOfEntities() {
        abstractRepository.load(project1, project2, project3);
        final List<Project> testProjectList = new ArrayList<>();
        testProjectList.add(project1);
        testProjectList.add(project2);
        testProjectList.add(project3);
        Assert.assertEquals(testProjectList, abstractRepository.findAll());
    }

}