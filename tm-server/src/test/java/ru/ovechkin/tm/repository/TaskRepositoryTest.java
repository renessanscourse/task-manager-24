package ru.ovechkin.tm.repository;

import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;
import ru.ovechkin.tm.api.repository.ITaskRepository;
import ru.ovechkin.tm.constant.TestConst;
import ru.ovechkin.tm.entity.Task;
import ru.ovechkin.tm.entitiesForTest.TasksForTest;

import java.util.List;

public class TaskRepositoryTest {

    private final ITaskRepository taskRepository = new TaskRepository();

    private  final TasksForTest tasksForTest = new TasksForTest();

    private final Task task1 = tasksForTest.getTask1();

    private final Task task2 = tasksForTest.getTask2();

    private  final Task task3 = tasksForTest.getTask3();

    public List<Task> listOfTestTasks() {
        return tasksForTest.getListOfTestTasks();
    }

    @Before
    public void clearRepository() {
        taskRepository.clear();
        taskRepository.merge(listOfTestTasks());
    }

    @Test
    public void testAdd() {
        taskRepository.clear();
        taskRepository.add(task1.getUserId(), task1);
        Assert.assertEquals(task1, taskRepository.findById(task1.getUserId(), task1.getId()));
    }

    @Test
    public void testFindUserTasks() {
        final List<Task> userOfTasks = listOfTestTasks();
        userOfTasks.remove(task3);// убираем таск с ненужным userId
        Assert.assertEquals(userOfTasks, taskRepository.findAllUserTask(TestConst.USER_ID_USER));
    }

    @Test
    public void testRemoveAll() {
        final List<Task> tasks = listOfTestTasks();
        tasks.removeAll(listOfTestTasks());
        taskRepository.removeAll(TestConst.USER_ID_USER);
        Assert.assertEquals(tasks, taskRepository.findAllUserTask(TestConst.USER_ID_USER));
    }

    @Test
    public void testFindById() {
        taskRepository.clear();
        taskRepository.merge(task1);
        Assert.assertEquals(task1, taskRepository.findById(task1.getUserId(), task1.getId()));
        Assert.assertNull(taskRepository.findById(task2.getUserId(), task2.getId()));
        Assert.assertNull(taskRepository.findById(task2.getUserId(), task3.getId()));
    }

    @Test
    public void testFindByIndex() {
        Assert.assertEquals(task3, taskRepository.findByIndex(task3.getUserId(), 1));
        Assert.assertNull(taskRepository.findByIndex(task3.getUserId(), 2));
    }

    @Test
    public void testFindByName() {
        Assert.assertEquals(task3, taskRepository.findByName(task3.getUserId(), task3.getName()));
        Assert.assertNull(taskRepository.findByName(task3.getUserId(), "task3.getName())"));
        Assert.assertNull(taskRepository.findByName(task2.getUserId(), task3.getName()));
    }

    @Test
    public void testRemoveById() {
        taskRepository.removeById(task1.getUserId(), task1.getId());
        Assert.assertNull(taskRepository.findById(task1.getUserId(), task1.getId()));
    }

    @Test
    public void testRemoveByIndex() {
        taskRepository.removeByIndex(task1.getUserId(), 1);
        Assert.assertNull(taskRepository.findById(task1.getUserId(), task1.getId()));
    }

    @Test
    public void testRemoveByName() {
        taskRepository.removeByName(task1.getUserId(), task1.getName());
        Assert.assertNull(taskRepository.findByName(task1.getUserId(), task1.getName()));
    }

}