package ru.ovechkin.tm.service;

import org.junit.Assert;
import org.junit.Test;
import ru.ovechkin.tm.api.service.IPropertyService;
import ru.ovechkin.tm.entity.Session;
import ru.ovechkin.tm.enumirated.Role;
import ru.ovechkin.tm.util.SignatureUtil;

/**
 * На данный моменд сессионный сервис покрыт только позитивными тестами
 * в рамках оптимизации времени
 */
public class SessionServiceTest extends AbstractEntitiesForTest {

    {
        serviceLocator.getUserService().mergeOne(user);
    }

    private final Session openedSession = sessionService.open(user.getLogin(), "testUser");


    @Test
    public void testCheckDataAccessPositive() {
        Assert.assertTrue(sessionService.checkDataAccess(user.getLogin(), "testUser"));
        serviceLocator.getUserService().clear();
    }

    @Test
    public void testIsValidPositive() {
        Assert.assertTrue(sessionService.isValid(openedSession));
    }

    @Test
    public void testValidatePositive() {
        sessionService.validate(openedSession);
    }

    @Test
    public void testValidateWithRolePositive() {
        sessionService.validate(openedSession, Role.USER);
    }

    @Test
    public void testOpenPositive() {
        Assert.assertTrue(sessionRepository.contains(openedSession.getId()));
    }

    @Test
    public void testSignPositive() {
        session.setSignature("");
        final IPropertyService propertyService = serviceLocator.getPropertyService();
        final String salt = propertyService.getSessionSalt();
        final Integer cycle = propertyService.getSessionCycle();
        final String signature = SignatureUtil.sign(session, salt, cycle);
        session.setSignature("");
        sessionService.sign(session);
        Assert.assertEquals(signature, session.getSignature());
    }

    @Test
    public void testSignOutByLoginPositive() {
        sessionService.signOutByLogin(user.getLogin());
        Assert.assertFalse(sessionRepository.contains(openedSession.getId()));
    }

    @Test
    public void testSignOutByUserIdPositive() {
        final Session openedSession = sessionService.open(user.getLogin(), "testUser");
        sessionService.signOutByUserId(openedSession.getUserId());
        Assert.assertFalse(sessionRepository.contains(openedSession.getId()));
    }

    @Test
    public void testGetUserPositive() {
        final Session openedSession = sessionService.open(user.getLogin(), "testUser");
        Assert.assertEquals(user, sessionService.getUser(openedSession));
    }

    @Test
    public void testGetUserIdPositive() {
        Assert.assertEquals(user.getId(), sessionService.getUserId(openedSession));
    }

    @Test
    public void testGetListSessionPositive() {
        sessionService.open(user.getLogin(), "testUser");
        Assert.assertEquals(2, sessionService.getListSession(openedSession).size());
    }

    @Test
    public void testClosePositive() {
        Assert.assertTrue(sessionRepository.contains(openedSession.getId()));
        sessionService.close(openedSession);
        Assert.assertFalse(sessionRepository.contains(openedSession.getId()));
    }

    @Test
    public void testCloseAllPositive() {
        sessionService.open(user.getLogin(), "testUser");
        sessionService.closeAll(openedSession);
        Assert.assertEquals(0, sessionRepository.findByUserId(openedSession.getUserId()).size());
    }

}