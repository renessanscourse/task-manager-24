package ru.ovechkin.tm.service;

import ru.ovechkin.tm.api.locator.IServiceLocator;
import ru.ovechkin.tm.api.repository.IProjectRepository;
import ru.ovechkin.tm.api.repository.ISessionRepository;
import ru.ovechkin.tm.api.repository.ITaskRepository;
import ru.ovechkin.tm.api.repository.IUserRepository;
import ru.ovechkin.tm.api.service.*;
import ru.ovechkin.tm.entitiesForTest.ProjectsForTest;
import ru.ovechkin.tm.entitiesForTest.SessionsForTest;
import ru.ovechkin.tm.entitiesForTest.TasksForTest;
import ru.ovechkin.tm.entitiesForTest.UsersForTest;
import ru.ovechkin.tm.entity.Project;
import ru.ovechkin.tm.entity.Session;
import ru.ovechkin.tm.entity.Task;
import ru.ovechkin.tm.entity.User;
import ru.ovechkin.tm.locator.ServiceLocator;
import ru.ovechkin.tm.repository.ProjectRepository;
import ru.ovechkin.tm.repository.SessionRepository;
import ru.ovechkin.tm.repository.TaskRepository;
import ru.ovechkin.tm.repository.UserRepository;

import java.util.List;

public abstract class AbstractEntitiesForTest {

    protected final IServiceLocator serviceLocator = new ServiceLocator();


    protected final IProjectRepository projectRepository = new ProjectRepository();

    protected final AbstractService<Project> abstractService = new ProjectService(projectRepository);

    protected final IProjectService projectService = new ProjectService(projectRepository);


    protected final ISessionRepository sessionRepository = new SessionRepository();

    protected final ISessionService sessionService = new SessionService(serviceLocator, sessionRepository);


    protected final StorageService storageService = new StorageService(serviceLocator);


    protected final ITaskRepository taskRepository = new TaskRepository();

    protected final ITaskService taskService = new TaskService(taskRepository);


    protected final IUserRepository userRepository = new UserRepository();

    protected final IUserService userService = new UserService(userRepository);


    protected final IAuthService authService = new AuthService(userService);


    protected final IDomainService domainService = new DomainService(taskService, projectService, userService);


    protected final SessionsForTest sessionsForTest = new SessionsForTest();

    protected final Session session = sessionsForTest.getSessionOfTestUser();


    protected final TasksForTest tasksForTest = new TasksForTest();

    protected final List<Task> testTasks = tasksForTest.getListOfTestTasks();

    protected final Task task1 = tasksForTest.getTask1();


    protected final ProjectsForTest projectsForTest = new ProjectsForTest();

    protected final List<Project> testProjects = projectsForTest.getListOfTestProjects();

    protected final Project project1 = projectsForTest.getProject1();

    protected final Project project2 = projectsForTest.getProject2();

    protected final Project project3 = projectsForTest.getProject3();


    protected final UsersForTest usersForTest = new UsersForTest();

    protected final List<User> testUsers = usersForTest.getListOfUsers();

    protected final User user = usersForTest.getUserUser();

    protected final User admin = usersForTest.getUserAdmin();

}