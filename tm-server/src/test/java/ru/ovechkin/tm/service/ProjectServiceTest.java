package ru.ovechkin.tm.service;

import org.junit.*;
import org.mockito.Mockito;
import ru.ovechkin.tm.api.repository.IProjectRepository;
import ru.ovechkin.tm.api.service.IProjectService;
import ru.ovechkin.tm.constant.TestConst;
import ru.ovechkin.tm.entity.Project;
import ru.ovechkin.tm.exeption.empty.*;
import ru.ovechkin.tm.repository.ProjectRepository;

import java.util.ArrayList;
import java.util.List;

public class ProjectServiceTest extends AbstractEntitiesForTest {

    private List<Project> listOfTestProjects() {
        return projectsForTest.getListOfTestProjects();
    }

    @Before
    public void prepareService() {
        projectService.clear();
    }

    @Test
    public void testCreateWithNamePositive() {
        projectService.create(user.getId(), "projectName1");
        final Project createdProject = projectService.findProjectByName(user.getId(), "projectName1");
        Assert.assertNotNull(createdProject);
        Assert.assertEquals("projectName1", createdProject.getName());
    }

    @Test
    public void testCreateWithNameNegative() {
        Assert.assertThrows(
                UserEmptyException.class,
                () -> projectService.create(null, TestConst.TEST_STRING));
        Assert.assertThrows(
                UserEmptyException.class,
                () -> projectService.create("", TestConst.TEST_STRING));
        Assert.assertThrows(
                NameEmptyException.class,
                () -> projectService.create(TestConst.TEST_STRING, null));
        Assert.assertThrows(
                NameEmptyException.class,
                () -> projectService.create(TestConst.TEST_STRING, ""));
    }

    @Test
    public void testCreateWithNameAndDescriptionPositive() {
        projectService.create(user.getId(), "projectName1", "descriptionForProject1");
        final Project createdProject = projectService.findProjectByIndex(user.getId(), 1);
        Assert.assertNotNull(createdProject);
        Assert.assertEquals("descriptionForProject1", createdProject.getDescription());
    }

    @Test
    public void testCreateWithNameAndDescriptionNegative() {
        Assert.assertThrows(
                DescriptionEmptyException.class,
                () -> projectService.create(TestConst.TEST_STRING, TestConst.TEST_STRING, null));
        Assert.assertThrows(
                DescriptionEmptyException.class,
                () -> projectService.create(TestConst.TEST_STRING, TestConst.TEST_STRING, ""));
        Assert.assertThrows(
                UserEmptyException.class,
                () -> projectService.create(null, TestConst.TEST_STRING));
        Assert.assertThrows(
                UserEmptyException.class,
                () -> projectService.create("", TestConst.TEST_STRING));
        Assert.assertThrows(
                NameEmptyException.class,
                () -> projectService.create(TestConst.TEST_STRING, null));
        Assert.assertThrows(
                NameEmptyException.class,
                () -> projectService.create(TestConst.TEST_STRING, ""));
    }

    @Test
    public void testFindUserProjectsPositive() {
        final IProjectService mock = Mockito.mock(ProjectService.class);
        Mockito
                .when(mock.findUserProjects(project1.getUserId()))
                .thenReturn(listOfTestProjects());
        Assert.assertEquals(
                listOfTestProjects(),
                mock.findUserProjects(project1.getUserId()));
    }

    @Test
    public void testFindUserProjectsNegative() {
        Assert.assertThrows(
                UserEmptyException.class,
                () -> projectService.findUserProjects(null));
        Assert.assertThrows(
                UserEmptyException.class,
                () -> projectService.findUserProjects(""));
    }

    @Test
    public void testRemoveAllProjectsPositive() {
        projectService.mergeCollection(listOfTestProjects());
        projectService.removeAllProjects(project1.getUserId());
        Assert.assertEquals(
                new ArrayList<>(),
                projectService.findUserProjects(project1.getUserId()));
    }

    @Test
    public void testRemoveAllProjectsNegative() {
        Assert.assertThrows(
                UserEmptyException.class,
                () -> projectService.removeAllProjects(null));
        Assert.assertThrows(
                UserEmptyException.class,
                () -> projectService.removeAllProjects(""));
        Assert.assertThrows(
                ProjectListEmptyException.class,
                () -> projectService.removeAllProjects(project1.getUserId()));
        final IProjectRepository mockedRepository = Mockito.mock(ProjectRepository.class);
        Mockito.when(mockedRepository.findAll()).thenReturn(null);
        Assert.assertThrows(
                ProjectListEmptyException.class,
                () -> projectService.removeAllProjects(project1.getUserId()));
    }

    @Test
    public void testFindProjectByIdPositive() {
        projectService.mergeCollection(listOfTestProjects());
        Assert.assertEquals(
                project1,
                projectService.findProjectById(project1.getUserId(), project1.getId()));
    }

    @Test
    public void testFindProjectByIdNegative() {
        Assert.assertThrows(
                UserEmptyException.class,
                () -> projectService.findProjectById(null, TestConst.TEST_STRING));
        Assert.assertThrows(
                UserEmptyException.class,
                () -> projectService.findProjectById("", TestConst.TEST_STRING));
        Assert.assertThrows(
                ProjectListEmptyException.class,
                () -> projectService.findProjectById(project1.getUserId(), project1.getId()));
        final IProjectRepository mockedRepository = Mockito.mock(ProjectRepository.class);
        Mockito.when(mockedRepository.findAll()).thenReturn(null);
        Assert.assertThrows(
                ProjectListEmptyException.class,
                () -> projectService.findProjectById(project1.getUserId(), project1.getId()));
        Assert.assertThrows(
                IdEmptyException.class,
                () -> projectService.findProjectById(project1.getUserId(), null));
        Assert.assertThrows(
                IdEmptyException.class,
                () -> projectService.findProjectById(project1.getUserId(), ""));
    }

    @Test
    public void testFindProjectByIndexPositive() {
        projectService.mergeCollection(listOfTestProjects());
        Assert.assertEquals(
                project1,
                projectService.findProjectByIndex(project1.getUserId(), 1));
    }

    @Test
    public void testFindProjectByIndexNegative() {
        Assert.assertThrows(
                UserEmptyException.class,
                () -> projectService.findProjectByIndex(null, 2));
        Assert.assertThrows(
                UserEmptyException.class,
                () -> projectService.findProjectByIndex("", 2));
        Assert.assertThrows(
                ProjectListEmptyException.class,
                () -> projectService.findProjectByIndex(project1.getUserId(), 1));
        final IProjectRepository mockedRepository = Mockito.mock(ProjectRepository.class);
        Mockito.when(mockedRepository.findAll()).thenReturn(null);
        Assert.assertThrows(
                ProjectListEmptyException.class,
                () -> projectService.findProjectByIndex(project1.getUserId(), 1));
        Assert.assertThrows(
                IndexEmptyException.class,
                () -> projectService.findProjectByIndex(project1.getUserId(), null));
        Assert.assertThrows(
                IndexEmptyException.class,
                () -> projectService.findProjectByIndex(project1.getUserId(), 0));
        Assert.assertThrows(
                IndexEmptyException.class,
                () -> projectService.findProjectByIndex(project1.getUserId(), -22));
    }

    @Test
    public void findProjectByNamePositive() {
        projectService.mergeCollection(listOfTestProjects());
        Assert.assertEquals(
                project1,
                projectService.findProjectByName(project1.getUserId(), project1.getName()));
    }

    @Test
    public void findProjectByNameNegative() {
        Assert.assertThrows(
                UserEmptyException.class,
                () -> projectService.findProjectByName(null, project1.getName()));
        Assert.assertThrows(
                UserEmptyException.class,
                () -> projectService.findProjectByName("", project1.getName()));
        Assert.assertThrows(
                ProjectListEmptyException.class,
                () -> projectService.findProjectByName(project1.getUserId(), project1.getName()));
        final IProjectRepository mockedRepository = Mockito.mock(ProjectRepository.class);
        Mockito.when(mockedRepository.findAll()).thenReturn(null);
        Assert.assertThrows(
                ProjectListEmptyException.class,
                () -> projectService.findProjectByName(project1.getUserId(), project1.getName()));
        Assert.assertThrows(
                NameEmptyException.class,
                () -> projectService.findProjectByName(project1.getUserId(), null));
        Assert.assertThrows(
                NameEmptyException.class,
                () -> projectService.findProjectByName(project1.getUserId(), ""));
    }

    @Test
    public void testUpdateProjectByIdPositive() {
        projectService.mergeOne(project1);
        final Project updatedProject1 = projectService.updateProjectById(
                project1.getUserId(), project1.getId(), "newName", "newDescription"
        );
        projectService.updateProjectById(
                project1.getUserId(), project1.getId(), "newName", "newDescription"
        );
        Assert.assertEquals(updatedProject1, project1);
        Assert.assertEquals(updatedProject1.getDescription(), project1.getDescription());
    }

    @Test
    public void testUpdateProjectByIdNegative() {
        Assert.assertThrows(
                UserEmptyException.class,
                () -> projectService.updateProjectById(
                        null,
                        project1.getId(),
                        project1.getName(),
                        project1.getDescription()));
        Assert.assertThrows(
                UserEmptyException.class,
                () -> projectService.updateProjectById(
                        "",
                        project1.getId(),
                        project1.getName(),
                        project1.getDescription()));
        Assert.assertThrows(
                ProjectListEmptyException.class,
                () -> projectService.updateProjectById(
                        project1.getUserId(),
                        project1.getId(),
                        project1.getName(),
                        project1.getDescription()));
        final IProjectRepository mockedRepository = Mockito.mock(ProjectRepository.class);
        Mockito.when(mockedRepository.findAll()).thenReturn(null);
        Assert.assertThrows(
                ProjectListEmptyException.class,
                () -> projectService.updateProjectById(
                        project1.getUserId(),
                        project1.getId(),
                        project1.getName(),
                        project1.getDescription()));
        projectService.mergeOne(project1);
        Assert.assertThrows(
                IdEmptyException.class,
                () -> projectService.updateProjectById(
                        project1.getUserId(),
                        null,
                        project1.getName(),
                        project1.getDescription()));
        Assert.assertThrows(
                IdEmptyException.class,
                () -> projectService.updateProjectById(
                        project1.getUserId(),
                        "",
                        project1.getName(),
                        project1.getDescription()));
        Assert.assertThrows(
                NameEmptyException.class,
                () -> projectService.updateProjectById(
                        project1.getUserId(),
                        project1.getId(),
                        "",
                        project1.getDescription()));
        Assert.assertThrows(
                NameEmptyException.class,
                () -> projectService.updateProjectById(
                        project1.getUserId(),
                        project1.getId(),
                        null,
                        project1.getDescription()));
        Assert.assertThrows(
                DescriptionEmptyException.class,
                () -> projectService.updateProjectById(
                        project1.getUserId(),
                        project1.getId(),
                        "newName",
                        null));
        Assert.assertThrows(
                DescriptionEmptyException.class,
                () -> projectService.updateProjectById(
                        project1.getUserId(),
                        project1.getId(),
                        "newName",
                        ""));
    }

    @Test
    public void testUpdateProjectByIndexPositive() {
        projectService.mergeOne(project1);
        final Project updatedProject1 = projectService.updateProjectByIndex(
                project1.getUserId(), 1, "newName", "newDescription"
        );
        projectService.updateProjectByIndex(
                project1.getUserId(), 1, "newName", "newDescription"
        );
        Assert.assertEquals(updatedProject1, project1);
        Assert.assertEquals(updatedProject1.getDescription(), project1.getDescription());
    }

    @Test
    public void testUpdateProjectByIndexNegative() {
        Assert.assertThrows(
                UserEmptyException.class,
                () -> projectService.updateProjectByIndex(
                        null,
                        1,
                        project1.getName(),
                        project1.getDescription()));
        Assert.assertThrows(
                UserEmptyException.class,
                () -> projectService.updateProjectByIndex(
                        "",
                        1,
                        project1.getName(),
                        project1.getDescription()));
        Assert.assertThrows(
                ProjectListEmptyException.class,
                () -> projectService.updateProjectByIndex(
                        project1.getUserId(),
                        1,
                        project1.getName(),
                        project1.getDescription()));
        final IProjectRepository mockedRepository = Mockito.mock(ProjectRepository.class);
        Mockito.when(mockedRepository.findAll()).thenReturn(null);
        Assert.assertThrows(
                ProjectListEmptyException.class,
                () -> projectService.updateProjectByIndex(
                        project1.getUserId(),
                        1,
                        project1.getName(),
                        project1.getDescription()));
        projectService.mergeOne(project1);
        Assert.assertThrows(
                IndexEmptyException.class,
                () -> projectService.updateProjectByIndex(
                        project1.getUserId(),
                        null,
                        project1.getName(),
                        project1.getDescription()));
        Assert.assertThrows(
                IndexEmptyException.class,
                () -> projectService.updateProjectByIndex(
                        project1.getUserId(),
                        0,
                        project1.getName(),
                        project1.getDescription()));
        Assert.assertThrows(
                IndexEmptyException.class,
                () -> projectService.updateProjectByIndex(
                        project1.getUserId(),
                        -1,
                        project1.getName(),
                        project1.getDescription()));
        Assert.assertThrows(
                NameEmptyException.class,
                () -> projectService.updateProjectByIndex(
                        project1.getUserId(),
                        1,
                        "",
                        project1.getDescription()));
        Assert.assertThrows(
                NameEmptyException.class,
                () -> projectService.updateProjectByIndex(
                        project1.getUserId(),
                        1,
                        null,
                        project1.getDescription()));
        Assert.assertThrows(
                DescriptionEmptyException.class,
                () -> projectService.updateProjectByIndex(
                        project1.getUserId(),
                        1,
                        "newName",
                        null));
        Assert.assertThrows(
                DescriptionEmptyException.class,
                () -> projectService.updateProjectByIndex(
                        project1.getUserId(),
                        1,
                        "newName",
                        ""));
    }

    @Test
    public void testRemoveByIdPositive() {
        projectService.mergeCollection(listOfTestProjects());
        projectService.removeProjectById(project1.getUserId(), project1.getId());
        Assert.assertNull(projectService.findProjectById(project1.getUserId(), project1.getId()));
    }

    @Test
    public void testRemoveByIdNegative() {
        Assert.assertThrows(
                UserEmptyException.class,
                () -> projectService.removeProjectById(null, TestConst.TEST_STRING));
        Assert.assertThrows(
                UserEmptyException.class,
                () -> projectService.removeProjectById("", TestConst.TEST_STRING));
        Assert.assertThrows(
                ProjectListEmptyException.class,
                () -> projectService.removeProjectById(project1.getUserId(), project1.getId()));
        final IProjectRepository mockedRepository = Mockito.mock(ProjectRepository.class);
        Mockito.when(mockedRepository.findAll()).thenReturn(null);
        Assert.assertThrows(
                ProjectListEmptyException.class,
                () -> projectService.removeProjectById(project1.getUserId(), project1.getId()));
        Assert.assertThrows(
                IdEmptyException.class,
                () -> projectService.removeProjectById(project1.getUserId(), null));
        Assert.assertThrows(
                IdEmptyException.class,
                () -> projectService.removeProjectById(project1.getUserId(), ""));
    }

    @Test
    public void testRemoveProjectByIndexPositive() {
        projectService.mergeCollection(listOfTestProjects());
        projectService.removeProjectByIndex(project1.getUserId(), 1);
        Assert.assertNull(projectService.findProjectById(project1.getUserId(), project1.getId()));
    }

    @Test
    public void testRemoveProjectByIndexNegative() {
        Assert.assertThrows(
                UserEmptyException.class,
                () -> projectService.removeProjectByIndex(null, 2));
        Assert.assertThrows(
                UserEmptyException.class,
                () -> projectService.removeProjectByIndex("", 2));
        Assert.assertThrows(
                ProjectListEmptyException.class,
                () -> projectService.removeProjectByIndex(project1.getUserId(), 1));
        final IProjectRepository mockedRepository = Mockito.mock(ProjectRepository.class);
        Mockito.when(mockedRepository.findAll()).thenReturn(null);
        Assert.assertThrows(
                ProjectListEmptyException.class,
                () -> projectService.removeProjectByIndex(project1.getUserId(), 1));
        Assert.assertThrows(
                IndexEmptyException.class,
                () -> projectService.removeProjectByIndex(project1.getUserId(), null));
        Assert.assertThrows(
                IndexEmptyException.class,
                () -> projectService.removeProjectByIndex(project1.getUserId(), 0));
        Assert.assertThrows(
                IndexEmptyException.class,
                () -> projectService.removeProjectByIndex(project1.getUserId(), -22));
    }

    @Test
    public void testRemoveProjectByNamePositive() {
        projectService.mergeCollection(listOfTestProjects());
        projectService.removeProjectByName(project1.getUserId(), project1.getName());
        Assert.assertNull(projectService.findProjectById(project1.getUserId(), project1.getId()));
    }

    @Test
    public void testRemoveProjectByNameNegative() {
        Assert.assertThrows(
                UserEmptyException.class,
                () -> projectService.removeProjectByName(null, project1.getName()));
        Assert.assertThrows(
                UserEmptyException.class,
                () -> projectService.removeProjectByName("", project1.getName()));
        Assert.assertThrows(
                ProjectListEmptyException.class,
                () -> projectService.removeProjectByName(project1.getUserId(), project1.getName()));
        final IProjectRepository mockedRepository = Mockito.mock(ProjectRepository.class);
        Mockito.when(mockedRepository.findAll()).thenReturn(null);
        Assert.assertThrows(
                ProjectListEmptyException.class,
                () -> projectService.removeProjectByName(project1.getUserId(), project1.getName()));
        Assert.assertThrows(
                NameEmptyException.class,
                () -> projectService.removeProjectByName(project1.getUserId(), null));
        Assert.assertThrows(
                NameEmptyException.class,
                () -> projectService.removeProjectByName(project1.getUserId(), ""));
    }

}