package ru.ovechkin.tm.service;

import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;
import ru.ovechkin.tm.entity.Project;

import java.util.List;

public class AbstractServiceTest extends AbstractEntitiesForTest {

    public List<Project> listOfTestProjects() {
        return projectsForTest.getListOfTestProjects();
    }

    @Before
    public void prepareService() {
        abstractService.clear();
        abstractService.mergeCollection(listOfTestProjects());
    }

    @Test
    public void findAllTest() {
        Assert.assertEquals(listOfTestProjects(), abstractService.findAll());
    }

    @Test
    public void testLoadOneEntity() {
        abstractService.load(project1);
        Assert.assertTrue(abstractService.findAll().contains(project1));
    }

    @Test
    public void loadManyOfEntities() {
        abstractService.load(project1, project2, project3);
        Assert.assertTrue(abstractService.findAll().contains(project1));
        Assert.assertTrue(abstractService.findAll().contains(project2));
        Assert.assertTrue(abstractService.findAll().contains(project3));
    }

    @Test
    public void testMergeOne() {
        abstractService.clear();
        abstractService.mergeOne(project1);
        Assert.assertTrue(abstractService.findAll().contains(project1));
    }

    @Test
    public void testMergeVarargs() {
        abstractService.clear();
        abstractService.mergeVarargs(project1, project2, project3);
        Assert.assertTrue(abstractService.findAll().contains(project1));
        Assert.assertTrue(abstractService.findAll().contains(project2));
        Assert.assertTrue(abstractService.findAll().contains(project3));
    }

    @Test
    public void testMergeCollection() {
        Assert.assertEquals(listOfTestProjects(), abstractService.findAll());
    }

    @Test
    public void testClear() {
        abstractService.clear();
        Assert.assertTrue(abstractService.findAll().isEmpty());
    }
}