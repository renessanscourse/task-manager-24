package ru.ovechkin.tm.service;

import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;
import ru.ovechkin.tm.entity.User;
import ru.ovechkin.tm.enumirated.Role;

/**
 * На данный моменд сервис аутентификации покрыт только позитивными тестами
 * в рамках оптимизации времени
 */
public class AuthServiceTest extends AbstractEntitiesForTest {

    @Before
    public void prepareAuthService() {
        userService.clear();
    }

    @Test
    public void testLogin() {
        userService.mergeOne(user);
        authService.login(user.getLogin(), "testUser");
        Assert.assertTrue(authService.isAuth());
    }

    @Test
    public void testGetUserIdPositive() {
        userService.mergeOne(user);
        authService.login(user.getLogin(), "testUser");
        Assert.assertEquals(user.getId(), authService.getUserId());
    }

    @Test
    public void testCheckRolesPositive() {
        session.setUserId(user.getId());
        userService.mergeOne(user);
        authService.checkRoles(session, new Role[] {Role.USER});
    }

    @Test
    public void testIsAuthPositive() {
        userService.mergeOne(user);
        authService.login(user.getLogin(), "testUser");
        Assert.assertTrue(authService.isAuth());
    }

    @Test
    public void testLogoutPositive() {
        userService.mergeOne(user);
        authService.login(user.getLogin(), "testUser");
        Assert.assertTrue(authService.isAuth());
        authService.logout();
        Assert.assertFalse(authService.isAuth());
    }

    @Test
    public void testRegistryPositive() {
        Assert.assertNull(userRepository.findById(user.getLogin()));
        authService.registry(user.getLogin(), "testUser", user.getEmail());
        Assert.assertNotNull(userService.findByLogin(user.getLogin()));
    }

    @Test
    public void testFindByIdPositive() {
        user.setId("ConstantId");
        userService.mergeOne(user);
        Assert.assertEquals(user, authService.findUserByUserId(user.getId()));
    }

    @Test
    public void testUpdateProfilePositive() {
        userService.mergeOne(user);
        authService.login(user.getLogin(), "testUser");
        final User updatedUser =
                authService.updateProfileInfo(
                        "newLogin",
                        "newFirstName",
                        "newMiddleName",
                        "newLastname",
                        "newEmail");
        Assert.assertEquals(updatedUser, authService.findUserByUserId(authService.getUserId()));
    }

    @Test
    public void testUpdatePasswordPositive() {
        userService.mergeOne(user);
        authService.login(user.getLogin(), "testUser");
        authService.updatePassword("testUser", "changedPassword");
        authService.logout();
        authService.login(user.getLogin(), "changedPassword");
    }

}