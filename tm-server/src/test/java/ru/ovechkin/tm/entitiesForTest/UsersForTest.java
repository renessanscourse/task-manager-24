package ru.ovechkin.tm.entitiesForTest;

import ru.ovechkin.tm.entity.User;
import ru.ovechkin.tm.enumirated.Role;
import ru.ovechkin.tm.util.HashUtil;

import java.util.ArrayList;
import java.util.List;

public class UsersForTest {

    private User createUser(
            final String login,
            final String password,
            final String email,
            final String firstName,
            final String middleName,
            final String lastName
    ) {
        final User user = new User();
        user.setLogin(login);
        user.setPasswordHash(HashUtil.salt(password));
        user.setEmail(email);
        user.setFirstName(firstName);
        user.setMiddleName(middleName);
        user.setLastName(lastName);
        return user;
    }

    final User userUser = createUser(
            "testUser",
            "testUser",
            "user@test",
            "firstUser",
            "middleUser",
            "lastUser"
    );

    final User userAdmin = createUser(
            "testAdmin",
            "testAdmin",
            "admin@test",
            "firstAdmin",
            "secondAdmin",
            "lastAdmin"
    );

    {userAdmin.setRole(Role.ADMIN);}

    public List<User> getListOfUsers() {
        final List<User> userList = new ArrayList<>();
        userList.add(userUser);
        userList.add(userAdmin);
        return userList;
    }

    public User getUserUser() {
        return userUser;
    }

    public User getUserAdmin() {
        return userAdmin;
    }

}