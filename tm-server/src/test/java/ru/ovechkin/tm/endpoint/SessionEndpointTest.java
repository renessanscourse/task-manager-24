package ru.ovechkin.tm.endpoint;

import org.junit.Assert;
import org.junit.Test;
import ru.ovechkin.tm.api.endpoint.ISessionEndpoint;
import ru.ovechkin.tm.api.locator.IEndpointLocator;
import ru.ovechkin.tm.api.locator.IServiceLocator;
import ru.ovechkin.tm.entitiesForTest.UsersForTest;
import ru.ovechkin.tm.entity.Session;
import ru.ovechkin.tm.entity.User;
import ru.ovechkin.tm.exeption.other.AccessForbiddenException;
import ru.ovechkin.tm.locator.EndpointLocator;
import ru.ovechkin.tm.locator.ServiceLocator;

public class SessionEndpointTest {

    private final IServiceLocator serviceLocator = new ServiceLocator();

    private final IEndpointLocator endpointLocator = new EndpointLocator(serviceLocator);

    private final ISessionEndpoint sessionEndpoint = endpointLocator.getSessionEndpoint();


    private final UsersForTest usersForTest = new UsersForTest();

    private final User user = usersForTest.getUserUser();

    {
        serviceLocator.getUserService().clear();
        serviceLocator.getSessionService().clear();
        user.setId("ConstantUserId");
        serviceLocator.getUserService().mergeOne(user);
    }

    private final Session openedSession = sessionEndpoint.openSession(user.getLogin(), "testUser");

    @Test
    public void testOpenSessionPositive() {
        Assert.assertTrue(sessionEndpoint.isValid(openedSession));
    }

    @Test
    public void testCloseSessionPositive() {
        Assert.assertTrue(sessionEndpoint.isValid(openedSession));
        sessionEndpoint.closeSession(openedSession);
        Assert.assertThrows(
                AccessForbiddenException.class,
                () -> sessionEndpoint.isValid(openedSession));
    }

    @Test
    public void testSessionsOfUserPositive() {
        sessionEndpoint.openSession(user.getLogin(), "testUser");
        Assert.assertEquals(2, sessionEndpoint.sessionsOfUser(openedSession).size());
    }

    @Test
    public void testIsValidPositive() {
        Assert.assertTrue(sessionEndpoint.isValid(openedSession));
    }

    @Test
    public void testSignOutByLoginPositive() {
        Assert.assertTrue(sessionEndpoint.isValid(openedSession));
        sessionEndpoint.signOutByLogin(user.getLogin());
        Assert.assertThrows(
                AccessForbiddenException.class,
                () -> sessionEndpoint.isValid(openedSession));
    }

    @Test
    public void testSignOutByUserIdPositive() {
        Assert.assertTrue(sessionEndpoint.isValid(openedSession));
        sessionEndpoint.signOutByUserId(user.getId());
        Assert.assertThrows(
                AccessForbiddenException.class,
                () -> sessionEndpoint.isValid(openedSession));
    }

    @Test
    public void testGetUserPositive() {
        Assert.assertEquals(user, sessionEndpoint.getUser(openedSession));
    }

    @Test
    public void testCloseAllPositive() {
        sessionEndpoint.openSession(user.getLogin(), "testUser");
        sessionEndpoint.closeAll(openedSession);
        Assert.assertEquals(0, serviceLocator.getSessionService().findAll().size());
    }

}