package ru.ovechkin.tm.endpoint;

import org.junit.Assert;
import org.junit.Ignore;
import org.junit.Test;
import ru.ovechkin.tm.api.endpoint.IAuthEndpoint;
import ru.ovechkin.tm.api.endpoint.ISessionEndpoint;
import ru.ovechkin.tm.api.locator.IEndpointLocator;
import ru.ovechkin.tm.api.locator.IServiceLocator;
import ru.ovechkin.tm.entity.Session;
import ru.ovechkin.tm.entity.User;
import ru.ovechkin.tm.enumirated.Role;
import ru.ovechkin.tm.exeption.other.AccessForbiddenException;
import ru.ovechkin.tm.exeption.user.AlreadyLoggedInException;
import ru.ovechkin.tm.locator.EndpointLocator;
import ru.ovechkin.tm.locator.ServiceLocator;

public class AuthEndpointTest {

    private final IServiceLocator serviceLocator = new ServiceLocator();

    private final IEndpointLocator endpointLocator = new EndpointLocator(serviceLocator);

    private final ISessionEndpoint sessionEndpoint = endpointLocator.getSessionEndpoint();

    private final IAuthEndpoint authEndpoint = endpointLocator.getAuthEndpoint();


    final Session openedSession = sessionEndpoint.openSession("user", "user");

    @Test
    public void testGetUserId() {
        Assert.assertEquals(openedSession.getUserId(), authEndpoint.getUserId(openedSession));
    }

    @Test
    public void testLogin() {
        sessionEndpoint.closeSession(openedSession);
        Assert.assertThrows(
                AccessForbiddenException.class,
                () -> sessionEndpoint.isValid(openedSession));
        final Session sessionOfUser = authEndpoint.login("user", "user");
        Assert.assertThrows(
                AlreadyLoggedInException.class,
                () -> authEndpoint.login("user", "user")
        );
        authEndpoint.logout(sessionOfUser);
    }

    @Test
    public void testLogout() {
        sessionEndpoint.closeSession(openedSession);
        Assert.assertThrows(
                AccessForbiddenException.class,
                () -> sessionEndpoint.isValid(openedSession));
        final Session sessionOfUser = authEndpoint.login("user", "user");
        authEndpoint.logout(sessionOfUser);
        Assert.assertFalse(authEndpoint.isAuth(sessionOfUser));
    }

    @Test
    public void testRegistry() {
        authEndpoint.registry("test", "test", "test");
        final Session sessionOfUser = authEndpoint.login("test", "test");
        Assert.assertTrue(authEndpoint.isAuth(sessionOfUser));
        authEndpoint.logout(sessionOfUser);
    }

    @Test
    public void testShowCurrentUser() {
        final Session sessionOfUser = authEndpoint.login("user", "user");
        final User user = serviceLocator.getUserService().findByLogin("user");
        Assert.assertEquals(user, authEndpoint.showCurrentUserByUserId(sessionOfUser));
        authEndpoint.logout(sessionOfUser);
    }

    @Test
    public void testUpdateProfileInfo() {
        final Session sessionOfUser = authEndpoint.login("user", "user");
        authEndpoint.updateProfileInfo(
                sessionOfUser,
                "newLogin",
                "newFirstname",
                "newMiddleName",
                "newLastName",
                "newEmail");
        Assert.assertEquals("newLogin", authEndpoint.showCurrentUserByUserId(sessionOfUser).getLogin());
        Assert.assertEquals("newFirstname", authEndpoint.showCurrentUserByUserId(sessionOfUser).getFirstName());
        Assert.assertEquals("newMiddleName", authEndpoint.showCurrentUserByUserId(sessionOfUser).getMiddleName());
        Assert.assertEquals("newLastName", authEndpoint.showCurrentUserByUserId(sessionOfUser).getLastName());
        Assert.assertEquals("newEmail", authEndpoint.showCurrentUserByUserId(sessionOfUser).getEmail());
        authEndpoint.logout(sessionOfUser);
    }

    @Test
    @Ignore("Работает, но обновляет пароль тестового юзера, что блокирует выполнение других кейсов")
    public void testUpdatePassword() {
        final Session sessionOfUser = authEndpoint.login("user", "user");
        authEndpoint.updatePassword(sessionOfUser, "user", "user1");
        authEndpoint.logout(sessionOfUser);
        authEndpoint.login("user", "user1");
        authEndpoint.logout(sessionOfUser);
    }

    @Test
    public void testCheckRoles() {
        final Session sessionOfUser = authEndpoint.login("user", "user");
        authEndpoint.checkRoles(sessionOfUser, new Role[] {Role.USER});
        authEndpoint.logout(sessionOfUser);
    }

    @Test
    public void testIsAuth() {
        final Session sessionOfUser = authEndpoint.login("user", "user");
        Assert.assertTrue(authEndpoint.isAuth(sessionOfUser));
        authEndpoint.logout(sessionOfUser);
    }

}