package ru.ovechkin.tm.endpoint;

import org.junit.AfterClass;
import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;
import ru.ovechkin.tm.api.endpoint.IAuthEndpoint;
import ru.ovechkin.tm.api.endpoint.IStorageEndpoint;
import ru.ovechkin.tm.api.locator.IEndpointLocator;
import ru.ovechkin.tm.api.locator.IServiceLocator;
import ru.ovechkin.tm.constant.PathToSavedFile;
import ru.ovechkin.tm.entitiesForTest.ProjectsForTest;
import ru.ovechkin.tm.entitiesForTest.TasksForTest;
import ru.ovechkin.tm.entitiesForTest.UsersForTest;
import ru.ovechkin.tm.entity.Project;
import ru.ovechkin.tm.entity.Session;
import ru.ovechkin.tm.entity.Task;
import ru.ovechkin.tm.entity.User;
import ru.ovechkin.tm.enumirated.Role;
import ru.ovechkin.tm.locator.EndpointLocator;
import ru.ovechkin.tm.locator.ServiceLocator;

import javax.xml.bind.JAXBException;
import java.io.File;
import java.io.IOException;
import java.nio.file.Files;
import java.util.List;

public class StorageEndpointTest {

    private final IServiceLocator serviceLocator = new ServiceLocator();

    private final IEndpointLocator endpointLocator = new EndpointLocator(serviceLocator);

    private final IAuthEndpoint authEndpoint = endpointLocator.getAuthEndpoint();

    private final IStorageEndpoint storageEndpoint = new StorageEndpoint(serviceLocator);


    private final UsersForTest usersForTest = new UsersForTest();

    private final List<User> testUsers = usersForTest.getListOfUsers();


    private final ProjectsForTest projectsForTest = new ProjectsForTest();

    private final List<Project> testProjects = projectsForTest.getListOfTestProjects();


    private final TasksForTest tasksForTest = new TasksForTest();

    private final List<Task> testTasks = tasksForTest.getListOfTestTasks();


    @Before
    public void prepareUsers() {
        testUsers.get(0).setId("ConstantUserId");
        testUsers.get(1).setId("ConstantAdminId");
    }


    @AfterClass
    public static void removeSavedFiles() throws IOException {
        final File base64Save = new File(PathToSavedFile.DATA_BASE64);
        Files.deleteIfExists(base64Save.toPath());
        final File binSave = new File(PathToSavedFile.DATA_BIN);
        Files.deleteIfExists(binSave.toPath());
        final File jJSave = new File(PathToSavedFile.DATA_JSON_JAXB);
        Files.deleteIfExists(jJSave.toPath());
        final File jMSave = new File(PathToSavedFile.DATA_JSON_MAPPER);
        Files.deleteIfExists(jMSave.toPath());
        final File xJSave = new File(PathToSavedFile.DATA_XML_JAXB);
        Files.deleteIfExists(xJSave.toPath());
        final File xMSave = new File(PathToSavedFile.DATA_XML_MAPPER);
        Files.deleteIfExists(xMSave.toPath());
    }

    @Test
    public void testDataBase64ASave() throws Exception {
        serviceLocator.getUserService().clear();
        serviceLocator.getUserService().mergeCollection(testUsers);
        serviceLocator.getProjectService().mergeCollection(projectsForTest.getListOfTestProjects());
        serviceLocator.getTaskService().mergeCollection(tasksForTest.getListOfTestTasks());
        final Session adminSession = authEndpoint.login("testAdmin", "testAdmin");
        storageEndpoint.dataBase64Save(adminSession);
        final File file = new File(PathToSavedFile.DATA_BASE64);
        Assert.assertTrue(file.isFile());
    }

    @Test
    public void testDataBase64BLoad() throws Exception {
        serviceLocator.getUserService().clear();
        serviceLocator.getProjectService().clear();
        serviceLocator.getTaskService().clear();

        endpointLocator.getAuthEndpoint().registry("user2", "user2", "user2");
        final Session openedSession = authEndpoint.login("user2", "user2");
        endpointLocator
                .getUserEndpoint()
                .createWithLoginPasswordAndRole(openedSession, "test", "test", Role.ADMIN);
        authEndpoint.logout(openedSession);
        final Session adminSession = authEndpoint.login("test", "test");
        storageEndpoint.dataBase64Load(adminSession);

        Assert.assertEquals(testUsers.toString(), serviceLocator.getUserService().findAll().toString());
        Assert.assertEquals(testProjects.toString(), serviceLocator.getProjectService().findAll().toString());
        Assert.assertEquals(testTasks.toString(), serviceLocator.getTaskService().findAll().toString());
    }

    @Test
    public void testDataBinaryASave() throws IOException {
        serviceLocator.getUserService().clear();
        serviceLocator.getUserService().mergeCollection(testUsers);
        serviceLocator.getProjectService().mergeCollection(projectsForTest.getListOfTestProjects());
        serviceLocator.getTaskService().mergeCollection(tasksForTest.getListOfTestTasks());
        final Session adminSession = authEndpoint.login("testAdmin", "testAdmin");
        storageEndpoint.dataBinarySave(adminSession);
        final File file = new File(PathToSavedFile.DATA_BIN);
        Assert.assertTrue(file.isFile());
    }

    @Test
    public void testDataBinaryBLoad() throws IOException, ClassNotFoundException {
        serviceLocator.getUserService().clear();
        serviceLocator.getProjectService().clear();
        serviceLocator.getTaskService().clear();

        endpointLocator.getAuthEndpoint().registry("user2", "user2", "user2");
        final Session openedSession = authEndpoint.login("user2", "user2");
        endpointLocator
                .getUserEndpoint()
                .createWithLoginPasswordAndRole(openedSession, "test", "test", Role.ADMIN);
        authEndpoint.logout(openedSession);
        final Session adminSession = authEndpoint.login("test", "test");
        storageEndpoint.dataBinaryLoad(adminSession);


        Assert.assertEquals(testUsers.toString(), serviceLocator.getUserService().findAll().toString());
        Assert.assertEquals(testProjects.toString(), serviceLocator.getProjectService().findAll().toString());
        Assert.assertEquals(testTasks.toString(), serviceLocator.getTaskService().findAll().toString());
    }

    @Test
    public void testDataJsonJaxbASave() throws IOException, JAXBException {
        serviceLocator.getUserService().clear();
        serviceLocator.getUserService().mergeCollection(testUsers);
        serviceLocator.getProjectService().mergeCollection(projectsForTest.getListOfTestProjects());
        serviceLocator.getTaskService().mergeCollection(tasksForTest.getListOfTestTasks());
        final Session adminSession = authEndpoint.login("testAdmin", "testAdmin");
        storageEndpoint.dataJsonJaxbSave(adminSession);
        final File file = new File(PathToSavedFile.DATA_JSON_JAXB);
        Assert.assertTrue(file.isFile());
    }

    @Test
    public void testDataJsonJaxbBLoad() throws JAXBException {
        serviceLocator.getUserService().clear();
        serviceLocator.getProjectService().clear();
        serviceLocator.getTaskService().clear();

        endpointLocator.getAuthEndpoint().registry("user2", "user2", "user2");
        final Session openedSession = authEndpoint.login("user2", "user2");
        endpointLocator
                .getUserEndpoint()
                .createWithLoginPasswordAndRole(openedSession, "test", "test", Role.ADMIN);
        authEndpoint.logout(openedSession);
        final Session adminSession = authEndpoint.login("test", "test");
        storageEndpoint.dataJsonJaxbLoad(adminSession);


        Assert.assertEquals(testUsers.toString(), serviceLocator.getUserService().findAll().toString());
        Assert.assertEquals(testProjects.toString(), serviceLocator.getProjectService().findAll().toString());
        Assert.assertEquals(testTasks.toString(), serviceLocator.getTaskService().findAll().toString());
    }

    @Test
    public void testDataJsonMapperBLoad() throws IOException {
        serviceLocator.getUserService().clear();
        serviceLocator.getProjectService().clear();
        serviceLocator.getTaskService().clear();

        endpointLocator.getAuthEndpoint().registry("user2", "user2", "user2");
        final Session openedSession = authEndpoint.login("user2", "user2");
        endpointLocator
                .getUserEndpoint()
                .createWithLoginPasswordAndRole(openedSession, "test", "test", Role.ADMIN);
        authEndpoint.logout(openedSession);
        final Session adminSession = authEndpoint.login("test", "test");
        storageEndpoint.dataJsonMapperLoad(adminSession);


        Assert.assertEquals(testUsers.toString(), serviceLocator.getUserService().findAll().toString());
        Assert.assertEquals(testProjects.toString(), serviceLocator.getProjectService().findAll().toString());
        Assert.assertEquals(testTasks.toString(), serviceLocator.getTaskService().findAll().toString());
    }

    @Test
    public void testDataJsonMapperASave() throws IOException {
        serviceLocator.getUserService().clear();
        serviceLocator.getUserService().mergeCollection(testUsers);
        serviceLocator.getProjectService().mergeCollection(projectsForTest.getListOfTestProjects());
        serviceLocator.getTaskService().mergeCollection(tasksForTest.getListOfTestTasks());
        final Session adminSession = authEndpoint.login("testAdmin", "testAdmin");
        storageEndpoint.dataJsonMapperSave(adminSession);
        final File file = new File(PathToSavedFile.DATA_JSON_MAPPER);
        Assert.assertTrue(file.isFile());
    }

    @Test
    public void testDataXmlJaxbASave() throws IOException, JAXBException {
        serviceLocator.getUserService().clear();
        serviceLocator.getUserService().mergeCollection(testUsers);
        serviceLocator.getProjectService().mergeCollection(projectsForTest.getListOfTestProjects());
        serviceLocator.getTaskService().mergeCollection(tasksForTest.getListOfTestTasks());

        final Session adminSession = authEndpoint.login("testAdmin", "testAdmin");
        storageEndpoint.dataXmlJaxbSave(adminSession);

        final File file = new File(PathToSavedFile.DATA_XML_JAXB);
        Assert.assertTrue(file.isFile());
    }

    @Test
    public void testDataXmlJaxbBLoad() throws IOException, JAXBException {
        serviceLocator.getUserService().clear();
        serviceLocator.getProjectService().clear();
        serviceLocator.getTaskService().clear();
        serviceLocator.getTaskService().clear();

        endpointLocator.getAuthEndpoint().registry("user2", "user2", "user2");
        final Session openedSession = authEndpoint.login("user2", "user2");
        endpointLocator
                .getUserEndpoint()
                .createWithLoginPasswordAndRole(openedSession, "test", "test", Role.ADMIN);
        authEndpoint.logout(openedSession);
        final Session adminSession = authEndpoint.login("test", "test");
        storageEndpoint.dataXmlJaxbLoad(adminSession);

        Assert.assertEquals(testUsers.toString(), serviceLocator.getUserService().findAll().toString());
        Assert.assertEquals(testProjects.toString(), serviceLocator.getProjectService().findAll().toString());
        Assert.assertEquals(testTasks.toString(), serviceLocator.getTaskService().findAll().toString());
    }

    @Test
    public void testDataXmlMapperASave() throws IOException {
        serviceLocator.getUserService().clear();
        serviceLocator.getUserService().mergeCollection(testUsers);
        serviceLocator.getProjectService().mergeCollection(projectsForTest.getListOfTestProjects());
        serviceLocator.getTaskService().mergeCollection(tasksForTest.getListOfTestTasks());
        final Session adminSession = authEndpoint.login("testAdmin", "testAdmin");
        storageEndpoint.dataXmlMapperSave(adminSession);
        final File file = new File(PathToSavedFile.DATA_XML_MAPPER);
        Assert.assertTrue(file.isFile());
    }

    @Test
    public void testDataXmlMapperBLoad() throws IOException {
        serviceLocator.getUserService().clear();
        serviceLocator.getProjectService().clear();
        serviceLocator.getTaskService().clear();

        endpointLocator.getAuthEndpoint().registry("user2", "user2", "user2");
        final Session openedSession = authEndpoint.login("user2", "user2");
        endpointLocator
                .getUserEndpoint()
                .createWithLoginPasswordAndRole(openedSession, "test", "test", Role.ADMIN);
        authEndpoint.logout(openedSession);
        final Session adminSession = authEndpoint.login("test", "test");
        storageEndpoint.dataXmlMapperLoad(adminSession);

        Assert.assertEquals(testUsers.toString(), serviceLocator.getUserService().findAll().toString());
        Assert.assertEquals(testProjects.toString(), serviceLocator.getProjectService().findAll().toString());
        Assert.assertEquals(testTasks.toString(), serviceLocator.getTaskService().findAll().toString());
    }

}