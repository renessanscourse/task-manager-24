package ru.ovechkin.tm.endpoint;

import org.junit.Assert;
import org.junit.Test;
import ru.ovechkin.tm.api.endpoint.IAuthEndpoint;
import ru.ovechkin.tm.api.endpoint.IProjectEndpoint;
import ru.ovechkin.tm.api.locator.IEndpointLocator;
import ru.ovechkin.tm.api.locator.IServiceLocator;
import ru.ovechkin.tm.entity.Session;
import ru.ovechkin.tm.exeption.empty.ProjectListEmptyException;
import ru.ovechkin.tm.locator.EndpointLocator;
import ru.ovechkin.tm.locator.ServiceLocator;

public class ProjectEndpointTest {

    private final IServiceLocator serviceLocator = new ServiceLocator();

    private final IEndpointLocator endpointLocator = new EndpointLocator(serviceLocator);

    private final IAuthEndpoint authEndpoint = endpointLocator.getAuthEndpoint();

    private final IProjectEndpoint projectEndpoint = endpointLocator.getProjectEndpoint();


    @Test
    public void testCreatePositive() {
        final Session openedSession = authEndpoint.login("user", "user");
        projectEndpoint.createProjectWithName(openedSession, "testProject");
        Assert.assertNotNull(projectEndpoint.findProjectByIndex(openedSession, 1));
        projectEndpoint.removeProjectByIndex(openedSession, 1);
        Assert.assertThrows(
                ProjectListEmptyException.class,
                () -> projectEndpoint.findProjectByIndex(openedSession, 1));
    }

    @Test
    public void testCreateWithDescription() {
        final Session openedSession = authEndpoint.login("user", "user");
        projectEndpoint.createProjectWithNameAndDescription(openedSession, "testProject", "description");
        Assert.assertNotNull(projectEndpoint.findProjectByIndex(openedSession, 1));
        projectEndpoint.removeProjectByIndex(openedSession, 1);
        Assert.assertThrows(
                ProjectListEmptyException.class,
                () -> projectEndpoint.findProjectByIndex(openedSession, 1));
    }

    @Test
    public void testShowListOfUserProjects() {
        final Session openedSession = authEndpoint.login("user", "user");
        projectEndpoint.createProjectWithName(openedSession, "testProject");
        projectEndpoint.createProjectWithName(openedSession, "test2");
        Assert.assertEquals(2, projectEndpoint.findUserProjects(openedSession).size());
        projectEndpoint.removeAllProjects(openedSession);
    }

    @Test
    public void testRemoveAllProjects() {
        final Session openedSession = authEndpoint.login("user", "user");
        projectEndpoint.createProjectWithName(openedSession, "testProject");
        projectEndpoint.createProjectWithName(openedSession, "test2");
        Assert.assertEquals(2, projectEndpoint.findUserProjects(openedSession).size());
        projectEndpoint.removeAllProjects(openedSession);
        Assert.assertEquals(0, projectEndpoint.findUserProjects(openedSession).size());
    }

    @Test
    public void testFindProjectByIndex() {
        final Session openedSession = authEndpoint.login("user", "user");
        projectEndpoint.createProjectWithNameAndDescription(openedSession, "testProject", "description");
        Assert.assertNotNull(projectEndpoint.findProjectByIndex(openedSession, 1));
        projectEndpoint.removeProjectByIndex(openedSession, 1);
        Assert.assertThrows(
                ProjectListEmptyException.class,
                () -> projectEndpoint.findProjectByIndex(openedSession, 1));
    }

    @Test
    public void testFindProjectByName() {
        final Session openedSession = authEndpoint.login("user", "user");
        projectEndpoint.createProjectWithNameAndDescription(openedSession, "testProject", "description");
        Assert.assertNotNull(projectEndpoint.findProjectByName(openedSession, "testProject"));
        projectEndpoint.removeProjectByName(openedSession, "testProject");
        Assert.assertThrows(
                ProjectListEmptyException.class,
                () -> projectEndpoint.removeProjectByName(openedSession, "testProject"));
    }

    @Test
    public void testUpdateProjectByIndex() {
        final Session openedSession = authEndpoint.login("user", "user");
        projectEndpoint.createProjectWithNameAndDescription(openedSession, "testProject", "description");
        projectEndpoint.updateProjectByIndex(openedSession, 1, "newName", "newDescription");
        Assert.assertNotNull(projectEndpoint.findProjectByName(openedSession, "newName"));
        projectEndpoint.removeProjectByName(openedSession, "newName");
    }

    @Test
    public void testRemoveProjectByIndex() {
        final Session openedSession = authEndpoint.login("user", "user");
        projectEndpoint.createProjectWithNameAndDescription(openedSession, "testProject", "description");
        Assert.assertNotNull(projectEndpoint.findProjectByIndex(openedSession, 1));
        projectEndpoint.removeProjectByIndex(openedSession, 1);
        Assert.assertThrows(
                ProjectListEmptyException.class,
                () -> projectEndpoint.findProjectByIndex(openedSession, 1));
    }

    @Test
    public void testRemoveProjectByName() {
        final Session openedSession = authEndpoint.login("user", "user");
        projectEndpoint.createProjectWithNameAndDescription(openedSession, "testProject", "description");
        Assert.assertNotNull(projectEndpoint.findProjectByName(openedSession, "testProject"));
        projectEndpoint.removeProjectByName(openedSession, "testProject");
        Assert.assertThrows(
                ProjectListEmptyException.class,
                () -> projectEndpoint.removeProjectByName(openedSession, "testProject"));
    }

}