package ru.ovechkin.tm.endpoint;

import org.junit.Assert;
import org.junit.Test;
import ru.ovechkin.tm.api.endpoint.IAuthEndpoint;
import ru.ovechkin.tm.api.endpoint.ITaskEndpoint;
import ru.ovechkin.tm.api.locator.IEndpointLocator;
import ru.ovechkin.tm.api.locator.IServiceLocator;
import ru.ovechkin.tm.entity.Session;
import ru.ovechkin.tm.locator.EndpointLocator;
import ru.ovechkin.tm.locator.ServiceLocator;

public class TaskEndpointTest {

    private final IServiceLocator serviceLocator = new ServiceLocator();

    private final IEndpointLocator endpointLocator = new EndpointLocator(serviceLocator);

    private final IAuthEndpoint authEndpoint = endpointLocator.getAuthEndpoint();

    private final ITaskEndpoint taskEndpoint = endpointLocator.getTaskEndpoint();


    @Test
    public void testCreatePositive() {
        final Session openedSession = authEndpoint.login("user", "user");
        taskEndpoint.createTaskWithName(openedSession, "testTask");
        Assert.assertNotNull(taskEndpoint.findTaskByIndex(openedSession, 1));
        taskEndpoint.removeTaskByIndex(openedSession, 1);
        Assert.assertNull(taskEndpoint.findTaskByIndex(openedSession, 1));
    }

    @Test
    public void testCreateWithDescription() {
        final Session openedSession = authEndpoint.login("user", "user");
        taskEndpoint.createTaskWithNameAndDescription(openedSession, "testTask", "description");
        Assert.assertNotNull(taskEndpoint.findTaskByIndex(openedSession, 1));
        taskEndpoint.removeTaskByIndex(openedSession, 1);
        Assert.assertNull(taskEndpoint.findTaskByIndex(openedSession, 1));

    }

    @Test
    public void testShowListOfUserTasks() {
        final Session openedSession = authEndpoint.login("user", "user");
        taskEndpoint.createTaskWithName(openedSession, "testTask");
        taskEndpoint.createTaskWithName(openedSession, "test2");
        Assert.assertEquals(2, taskEndpoint.findUserTasks(openedSession).size());
        taskEndpoint.removeAllTasks(openedSession);
    }

    @Test
    public void testRemoveAllTasks() {
        final Session openedSession = authEndpoint.login("user", "user");
        taskEndpoint.createTaskWithName(openedSession, "testTask");
        taskEndpoint.createTaskWithName(openedSession, "test2");
        Assert.assertEquals(2, taskEndpoint.findUserTasks(openedSession).size());
        taskEndpoint.removeAllTasks(openedSession);
        Assert.assertEquals(0, taskEndpoint.findUserTasks(openedSession).size());
    }

    @Test
    public void testFindTaskByIndex() {
        final Session openedSession = authEndpoint.login("user", "user");
        taskEndpoint.createTaskWithNameAndDescription(openedSession, "testTask", "description");
        Assert.assertNotNull(taskEndpoint.findTaskByIndex(openedSession, 1));
        taskEndpoint.removeTaskByIndex(openedSession, 1);
        Assert.assertNull(taskEndpoint.findTaskByIndex(openedSession, 1));
    }

    @Test
    public void testFindTaskByName() {
        final Session openedSession = authEndpoint.login("user", "user");
        taskEndpoint.createTaskWithNameAndDescription(openedSession, "testTask", "description");
        Assert.assertNotNull(taskEndpoint.findTaskByName(openedSession, "testTask"));
        taskEndpoint.removeTaskByName(openedSession, "testTask");
        Assert.assertNull(taskEndpoint.findTaskByIndex(openedSession, 1));

    }

    @Test
    public void testUpdateTaskByIndex() {
        final Session openedSession = authEndpoint.login("user", "user");
        taskEndpoint.createTaskWithNameAndDescription(openedSession, "testTask", "description");
        taskEndpoint.updateTaskByIndex(openedSession, 1, "newName", "newDescription");
        Assert.assertNotNull(taskEndpoint.findTaskByName(openedSession, "newName"));
        taskEndpoint.removeTaskByName(openedSession, "newName");
    }

    @Test
    public void testRemoveTaskByIndex() {
        final Session openedSession = authEndpoint.login("user", "user");
        taskEndpoint.createTaskWithNameAndDescription(openedSession, "testTask", "description");
        Assert.assertNotNull(taskEndpoint.findTaskByIndex(openedSession, 1));
        taskEndpoint.removeTaskByIndex(openedSession, 1);
        Assert.assertNull(taskEndpoint.findTaskByIndex(openedSession, 1));
    }

    @Test
    public void testRemoveTaskByName() {
        final Session openedSession = authEndpoint.login("user", "user");
        taskEndpoint.createTaskWithNameAndDescription(openedSession, "testTask", "description");
        Assert.assertNotNull(taskEndpoint.findTaskByName(openedSession, "testTask"));
        taskEndpoint.removeTaskByName(openedSession, "testTask");
        Assert.assertNull(taskEndpoint.findTaskByIndex(openedSession, 1));
    }
    
}