package ru.ovechkin.tm.endpoint;

import org.junit.Assert;
import org.junit.Test;
import ru.ovechkin.tm.api.endpoint.ISessionEndpoint;
import ru.ovechkin.tm.api.endpoint.IUserEndpoint;
import ru.ovechkin.tm.api.locator.IEndpointLocator;
import ru.ovechkin.tm.api.locator.IServiceLocator;
import ru.ovechkin.tm.entitiesForTest.UsersForTest;
import ru.ovechkin.tm.entity.Session;
import ru.ovechkin.tm.entity.User;
import ru.ovechkin.tm.enumirated.Role;
import ru.ovechkin.tm.exeption.unknown.LoginUnknownException;
import ru.ovechkin.tm.exeption.user.AccessDeniedException;
import ru.ovechkin.tm.locator.EndpointLocator;
import ru.ovechkin.tm.locator.ServiceLocator;

public class UserEndpointTest {

    private final IServiceLocator serviceLocator = new ServiceLocator();

    private final IEndpointLocator endpointLocator = new EndpointLocator(serviceLocator);

    private final ISessionEndpoint sessionEndpoint = endpointLocator.getSessionEndpoint();

    private final IUserEndpoint userEndpoint = endpointLocator.getUserEndpoint();


    private final UsersForTest usersForTest = new UsersForTest();

    private final User user = usersForTest.getUserUser();

    private final User admin = usersForTest.getUserAdmin();

    {
        serviceLocator.getUserService().clear();
        serviceLocator.getSessionService().clear();
        user.setId("ConstantUserId");
        admin.setId("ConstantAdminId");
        serviceLocator.getUserService().mergeOne(user);
        serviceLocator.getUserService().mergeOne(admin);
    }

    @Test
    public void testFindByIdPositive() {
        final Session adminSession = sessionEndpoint.openSession("testAdmin", "testAdmin");
        Assert.assertEquals(user, userEndpoint.findById(adminSession, user.getId()));
    }

    @Test
    public void testFindByLoginPositive() {
        final Session adminSession = sessionEndpoint.openSession("testAdmin", "testAdmin");
        Assert.assertEquals(user, userEndpoint.findByLogin(adminSession, user.getLogin()));
    }

    @Test
    public void testCreateWithLoginAndPasswordPositive() {
        final Session openedUserSession = sessionEndpoint.openSession(user.getLogin(), "testUser");
        final User testUser = userEndpoint.createWithLoginAndPassword(openedUserSession, "test", "test");
        sessionEndpoint.closeSession(openedUserSession);
        final Session adminSession = sessionEndpoint.openSession("testAdmin", "testAdmin");
        Assert.assertEquals(testUser, userEndpoint.findByLogin(adminSession, "test"));
    }

    @Test
    public void testCreateWithEmailPositive() {
        final Session openedUserSession = sessionEndpoint.openSession(user.getLogin(), "testUser");
        final User testUser = userEndpoint.createWithLoginPasswordAndEmail(
                openedUserSession, "test", "test", "test");
        sessionEndpoint.closeSession(openedUserSession);
        final Session adminSession = sessionEndpoint.openSession("testAdmin", "testAdmin");
        Assert.assertEquals(testUser, userEndpoint.findByLogin(adminSession, "test"));
    }

    @Test
    public void testCreateWithRolePositive() {
        final Session adminSession = sessionEndpoint.openSession("testAdmin", "testAdmin");
        final User testUser = userEndpoint.createWithLoginPasswordAndRole(
                adminSession, "test", "test", Role.ADMIN);
        Assert.assertEquals(testUser, userEndpoint.findByLogin(adminSession, "test"));
    }

    @Test
    public void testLockUserByLogin() {
        final Session adminSession = sessionEndpoint.openSession("testAdmin", "testAdmin");
        userEndpoint.lockUserByLogin(adminSession, user.getLogin());
        sessionEndpoint.closeSession(adminSession);
        Assert.assertThrows(
                AccessDeniedException.class,
                () -> serviceLocator.getAuthService().login(user.getLogin(), "testUser"));
    }

    @Test
    public void testUnlockUserByLogin() {
        final Session adminSession = sessionEndpoint.openSession("testAdmin", "testAdmin");
        userEndpoint.lockUserByLogin(adminSession, user.getLogin());
        Assert.assertThrows(
                AccessDeniedException.class,
                () -> serviceLocator.getAuthService().login(user.getLogin(), "testUser"));
        userEndpoint.unLockUserByLogin(adminSession, user.getLogin());
        serviceLocator.getAuthService().login(user.getLogin(), "testUser");
        serviceLocator.getAuthService().logout();
    }

    @Test
    public void testRemoveByIdPositive() {
        final Session adminSession = sessionEndpoint.openSession("testAdmin", "testAdmin");
        userEndpoint.removeById(adminSession, user.getId());
        Assert.assertThrows(
                LoginUnknownException.class,
                () -> serviceLocator.getAuthService().login(user.getLogin(), "testUser"));
    }

    @Test
    public void testRemoveByLoginPositive() {
        final Session adminSession = sessionEndpoint.openSession("testAdmin", "testAdmin");
        userEndpoint.removeByLogin(adminSession, user.getLogin());
        Assert.assertThrows(
                LoginUnknownException.class,
                () -> serviceLocator.getAuthService().login(user.getLogin(), "testUser"));
    }

}