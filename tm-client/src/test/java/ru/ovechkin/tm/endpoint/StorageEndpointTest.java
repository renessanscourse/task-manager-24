package ru.ovechkin.tm.endpoint;

import org.junit.AfterClass;
import org.junit.Assert;
import org.junit.Test;
import ru.ovechkin.tm.api.locator.IEndpointLocator;
import ru.ovechkin.tm.constant.PathToSavedFile;
import ru.ovechkin.tm.locator.EndpointLocator;

import java.io.File;
import java.io.IOException;
import java.nio.file.Files;

public class StorageEndpointTest {

    private final IEndpointLocator endpointLocator = new EndpointLocator();

    private final AuthEndpoint authEndpoint = endpointLocator.getAuthEndpoint();

    private final StorageEndpoint storageEndpoint = endpointLocator.getStorageEndpoint();

    @AfterClass
    public static void removeSavedFiles() throws IOException {
        final File base64Save = new File(PathToSavedFile.DATA_BASE64);
        Files.deleteIfExists(base64Save.toPath());
        final File binSave = new File(PathToSavedFile.DATA_BIN);
        Files.deleteIfExists(binSave.toPath());
        final File jJSave = new File(PathToSavedFile.DATA_JSON_JAXB);
        Files.deleteIfExists(jJSave.toPath());
        final File jMSave = new File(PathToSavedFile.DATA_JSON_MAPPER);
        Files.deleteIfExists(jMSave.toPath());
        final File xJSave = new File(PathToSavedFile.DATA_XML_JAXB);
        Files.deleteIfExists(xJSave.toPath());
        final File xMSave = new File(PathToSavedFile.DATA_XML_MAPPER);
        Files.deleteIfExists(xMSave.toPath());
    }

    @Test
    public void testDataBinASave() throws IOException_Exception {
        final Session adminSession = authEndpoint.login("admin", "admin");
        endpointLocator.getTaskEndpoint().removeAllTasks(adminSession);
        Assert.assertEquals(0, endpointLocator.getTaskEndpoint().findUserTasks(adminSession).size());
        endpointLocator.getTaskEndpoint().createTaskWithName(adminSession, "adminTest");
        storageEndpoint.dataBinarySave(adminSession);
        final File file = new File("C:/Users/ovech/task-manager-23/data.bin");
        Assert.assertTrue(file.isFile());
        endpointLocator.getTaskEndpoint().removeAllTasks(adminSession);
        authEndpoint.logout(adminSession);
    }

    @Test
    public void testDataBinBLoad() throws ClassNotFoundException_Exception, IOException_Exception {
        final Session adminSession = authEndpoint.login("admin", "admin");
        Assert.assertEquals(0, endpointLocator.getTaskEndpoint().findUserTasks(adminSession).size());
        endpointLocator.getStorageEndpoint().dataBinaryLoad(adminSession);
        Assert.assertEquals(1, endpointLocator.getTaskEndpoint().findUserTasks(adminSession).size());
        authEndpoint.logout(adminSession);
    }

    @Test
    public void testDataBase64ASave() throws Exception_Exception {
        final Session adminSession = authEndpoint.login("admin", "admin");
        endpointLocator.getTaskEndpoint().removeAllTasks(adminSession);
        Assert.assertEquals(0, endpointLocator.getTaskEndpoint().findUserTasks(adminSession).size());
        endpointLocator.getTaskEndpoint().createTaskWithName(adminSession, "adminTest");
        storageEndpoint.dataBase64Save(adminSession);
        final File file = new File("C:/Users/ovech/task-manager-23/data.base64");
        Assert.assertTrue(file.isFile());
        endpointLocator.getTaskEndpoint().removeAllTasks(adminSession);
        authEndpoint.logout(adminSession);
    }

    @Test
    public void testDataBase64BLoad() throws Exception_Exception {
        final Session adminSession = authEndpoint.login("admin", "admin");
        Assert.assertEquals(0, endpointLocator.getTaskEndpoint().findUserTasks(adminSession).size());
        endpointLocator.getStorageEndpoint().dataBase64Load(adminSession);
        Assert.assertEquals(1, endpointLocator.getTaskEndpoint().findUserTasks(adminSession).size());
        authEndpoint.logout(adminSession);
    }


    @Test
    public void testDataJsonJaxbASave() throws JAXBException_Exception, IOException_Exception {
        final Session adminSession = authEndpoint.login("admin", "admin");
        endpointLocator.getTaskEndpoint().removeAllTasks(adminSession);
        Assert.assertEquals(0, endpointLocator.getTaskEndpoint().findUserTasks(adminSession).size());
        endpointLocator.getTaskEndpoint().createTaskWithName(adminSession, "adminTest");
        storageEndpoint.dataJsonJaxbSave(adminSession);
        final File file = new File("C:/Users/ovech/task-manager-23/dataJaxb.json");
        Assert.assertTrue(file.isFile());
        endpointLocator.getTaskEndpoint().removeAllTasks(adminSession);
        authEndpoint.logout(adminSession);
    }

    @Test
    public void testDataJsonJaxbBLoad() throws JAXBException_Exception {
        final Session adminSession = authEndpoint.login("admin", "admin");
        Assert.assertEquals(0, endpointLocator.getTaskEndpoint().findUserTasks(adminSession).size());
        endpointLocator.getStorageEndpoint().dataJsonJaxbLoad(adminSession);
        Assert.assertEquals(1, endpointLocator.getTaskEndpoint().findUserTasks(adminSession).size());
        authEndpoint.logout(adminSession);
    }

    @Test
    public void testDataJsonMapperASave() throws IOException_Exception {
        final Session adminSession = authEndpoint.login("admin", "admin");
        endpointLocator.getTaskEndpoint().removeAllTasks(adminSession);
        Assert.assertEquals(0, endpointLocator.getTaskEndpoint().findUserTasks(adminSession).size());endpointLocator.getTaskEndpoint().createTaskWithName(adminSession, "adminTest");
        storageEndpoint.dataJsonMapperSave(adminSession);
        final File file = new File("C:/Users/ovech/task-manager-23/dataMapper.json");
        Assert.assertTrue(file.isFile());
        endpointLocator.getTaskEndpoint().removeAllTasks(adminSession);
        authEndpoint.logout(adminSession);
    }

    @Test
    public void testDataJsonMapperBLoad() throws IOException_Exception {
        final Session adminSession = authEndpoint.login("admin", "admin");
        Assert.assertEquals(0, endpointLocator.getTaskEndpoint().findUserTasks(adminSession).size());
        endpointLocator.getStorageEndpoint().dataJsonMapperLoad(adminSession);
        Assert.assertEquals(1, endpointLocator.getTaskEndpoint().findUserTasks(adminSession).size());
        authEndpoint.logout(adminSession);
    }

    @Test
    public void testDataXmlJaxbASave() throws IOException_Exception, JAXBException_Exception {
        final Session adminSession = authEndpoint.login("admin", "admin");
        endpointLocator.getTaskEndpoint().removeAllTasks(adminSession);
        Assert.assertEquals(0, endpointLocator.getTaskEndpoint().findUserTasks(adminSession).size());endpointLocator.getTaskEndpoint().createTaskWithName(adminSession, "adminTest");
        storageEndpoint.dataXmlJaxbSave(adminSession);
        final File file = new File("C:/Users/ovech/task-manager-23/dataJaxb.xml");
        Assert.assertTrue(file.isFile());
        endpointLocator.getTaskEndpoint().removeAllTasks(adminSession);
        authEndpoint.logout(adminSession);
    }

    @Test
    public void testDataXmlJaxbBLoad() throws IOException_Exception, JAXBException_Exception {
        final Session adminSession = authEndpoint.login("admin", "admin");
        Assert.assertEquals(0, endpointLocator.getTaskEndpoint().findUserTasks(adminSession).size());
        endpointLocator.getStorageEndpoint().dataXmlJaxbLoad(adminSession);
        Assert.assertEquals(1, endpointLocator.getTaskEndpoint().findUserTasks(adminSession).size());
        authEndpoint.logout(adminSession);
    }

    @Test
    public void testDataXmlMapperASave() throws IOException_Exception {
        final Session adminSession = authEndpoint.login("admin", "admin");
        endpointLocator.getTaskEndpoint().removeAllTasks(adminSession);
        Assert.assertEquals(0, endpointLocator.getTaskEndpoint().findUserTasks(adminSession).size());endpointLocator.getTaskEndpoint().createTaskWithName(adminSession, "adminTest");
        storageEndpoint.dataXmlMapperSave(adminSession);
        final File file = new File("C:/Users/ovech/task-manager-23/dataMapper.xml");
        Assert.assertTrue(file.isFile());
        endpointLocator.getTaskEndpoint().removeAllTasks(adminSession);
        authEndpoint.logout(adminSession);
    }

    @Test
    public void testDataXmlMapperBLoad() throws IOException_Exception {
        final Session adminSession = authEndpoint.login("admin", "admin");
        Assert.assertEquals(0, endpointLocator.getTaskEndpoint().findUserTasks(adminSession).size());
        endpointLocator.getStorageEndpoint().dataXmlMapperLoad(adminSession);
        Assert.assertEquals(1, endpointLocator.getTaskEndpoint().findUserTasks(adminSession).size());
        authEndpoint.logout(adminSession);
    }

}