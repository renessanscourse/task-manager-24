package ru.ovechkin.tm.exeption.unknown;

public class IdUnknownException extends RuntimeException {

    public IdUnknownException() {
        super("Error! This ID does not exist.");
    }

    public IdUnknownException(final String id) {
        super("Error! This ID [" + id + "] does not exist.");
    }

}
