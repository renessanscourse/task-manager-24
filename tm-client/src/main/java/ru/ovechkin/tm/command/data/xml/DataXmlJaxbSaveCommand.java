package ru.ovechkin.tm.command.data.xml;

import ru.ovechkin.tm.command.AbstractCommand;
import ru.ovechkin.tm.constant.CmdConst;

public class DataXmlJaxbSaveCommand extends AbstractCommand {

    @Override
    public String arg() {
        return null;
    }

    @Override
    public String name() {
        return CmdConst.DATA_XML_JAXB_SAVE;
    }

    @Override
    public String description() {
        return "Save data to xml file";
    }

    @Override
    public void execute() throws Exception {
        System.out.println("[DATA XML SAVE]");
        endpointLocator.getStorageEndpoint().dataXmlJaxbSave(session);
        System.out.println("[OK]");
    }

}