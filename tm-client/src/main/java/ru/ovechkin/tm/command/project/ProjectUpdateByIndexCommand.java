package ru.ovechkin.tm.command.project;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.ovechkin.tm.command.AbstractCommand;
import ru.ovechkin.tm.constant.CmdConst;
import ru.ovechkin.tm.endpoint.Project;
import ru.ovechkin.tm.util.TerminalUtil;

public final class ProjectUpdateByIndexCommand extends AbstractCommand {

    @Nullable
    @Override
    public String arg() {
        return null;
    }

    @NotNull
    @Override
    public String name() {
        return CmdConst.PROJECT_UPDATE_BY_INDEX;
    }

    @NotNull
    @Override
    public String description() {
        return "Update project by index";
    }

    @Override
    public void execute() {
        System.out.println("[UPDATE PROJECT]");
        System.out.print("ENTER PROJECT INDEX: ");
        @Nullable final Integer index = TerminalUtil.nextNumber() - 1;
        @NotNull final Project project =
                endpointLocator.getProjectEndpoint().findProjectByIndex(session, index);
        if (project == null) {
            System.out.println("[FAIL]");
            return;
        }
        System.out.print("ENTER NEW PROJECT NAME: ");
        @Nullable final String name = TerminalUtil.nextLine();
        System.out.print("ENTER NEW PROJECT DESCRIPTION: ");
        @Nullable final String description = TerminalUtil.nextLine();
        @NotNull final Project projectUpdated = endpointLocator.getProjectEndpoint()
                .updateProjectByIndex(session, index, name, description);
        if (projectUpdated == null) {
            System.out.println("[FAIL]");
            return;
        }
        System.out.println("[COMPLETE]");
    }

}