package ru.ovechkin.tm.command.auth;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.ovechkin.tm.command.AbstractCommand;
import ru.ovechkin.tm.constant.CmdConst;
import ru.ovechkin.tm.endpoint.User;
import ru.ovechkin.tm.util.TerminalUtil;

public class UserUpdatePasswordCommand extends AbstractCommand {

    @Nullable
    @Override
    public String arg() {
        return null;
    }

    @NotNull
    @Override
    public String name() {
        return CmdConst.UPDATE_PASSWORD;
    }

    @NotNull
    @Override
    public String description() {
        return "Update password to your account";
    }

    @Override
    public void execute() {
        @Nullable final String userId = endpointLocator.getAuthEndpoint().getUserId(session);
        @Nullable final User user = endpointLocator.getUserEndpoint().findById(session, userId);
        if (user == null) {
            System.out.println("[FAIL]");
            return;
        }
        System.out.println("[PASSWORD CHANGE]");
        System.out.print("ENTER YOUR CURRENT PASSWORD: ");
        @Nullable final String currentPassword = TerminalUtil.nextLine();
        System.out.print("ENTER NEW PASSWORD: ");
        @Nullable final String newPassword = TerminalUtil.nextLine();
        endpointLocator.getAuthEndpoint().updatePassword(session, currentPassword, newPassword);
    }

}