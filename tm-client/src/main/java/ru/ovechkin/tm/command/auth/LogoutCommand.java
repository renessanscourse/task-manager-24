package ru.ovechkin.tm.command.auth;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.ovechkin.tm.bootstrap.Bootstrap;
import ru.ovechkin.tm.command.AbstractCommand;
import ru.ovechkin.tm.constant.CmdConst;
import ru.ovechkin.tm.endpoint.AuthEndpoint;

public class LogoutCommand extends AbstractCommand {

    @Nullable
    @Override
    public String arg() {
        return null;
    }

    @NotNull
    @Override
    public String name() {
        return CmdConst.LOGOUT;
    }

    @NotNull
    @Override
    public String description() {
        return "Logout from your account";
    }

    @Override
    public void execute() {
        System.out.println("[LOGOUT]");
        @NotNull final AuthEndpoint authEndpoint = endpointLocator.getAuthEndpoint();
        authEndpoint.logout(Bootstrap.getSession());
        session.setId("");
        session.setUserId("");
        session.setSignature("");
        session.setTimestamp(0L);
        System.out.println("[OK]");
    }

}