package ru.ovechkin.tm.command.data.binary;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.ovechkin.tm.command.AbstractCommand;
import ru.ovechkin.tm.constant.CmdConst;

public class DataBinaryLoadCommand extends AbstractCommand {

    @Nullable
    @Override
    public String arg() {
        return null;
    }

    @NotNull
    @Override
    public String name() {
        return CmdConst.DATA_BIN_LOAD;
    }

    @NotNull
    @Override
    public String description() {
        return "Load data from binary file";
    }

    @Override
    public void execute() throws Exception {
        System.out.println("[DATA BINARY LOAD]");
        endpointLocator.getStorageEndpoint().dataBinaryLoad(session);
        System.out.println("[OK]");
    }

}