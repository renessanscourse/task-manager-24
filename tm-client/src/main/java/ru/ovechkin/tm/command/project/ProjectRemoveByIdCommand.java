package ru.ovechkin.tm.command.project;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.ovechkin.tm.command.AbstractCommand;
import ru.ovechkin.tm.constant.CmdConst;
import ru.ovechkin.tm.endpoint.Project;
import ru.ovechkin.tm.util.TerminalUtil;

public final class ProjectRemoveByIdCommand extends AbstractCommand {

    @Nullable
    @Override
    public String arg() {
        return null;
    }

    @NotNull
    @Override
    public String name() {
        return CmdConst.PROJECT_REMOVE_BY_ID;
    }

    @NotNull
    @Override
    public String description() {
        return "Remove project by id";
    }

    @Override
    public void execute() {
        System.out.println("[REMOVE PROJECT]");
        System.out.print("ENTER PROJECT ID: ");
        @Nullable final String id = TerminalUtil.nextLine();
        @NotNull final Project project =
                endpointLocator.getProjectEndpoint().removeProjectById(session, id);
        if (project == null) System.out.println("[FAIL]");
        else System.out.println("[COMPLETE]");
    }

}